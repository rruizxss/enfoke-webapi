import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './practice.reducer';
import { IPractice } from 'app/shared/model/practice.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPracticeDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class PracticeDetail extends React.Component<IPracticeDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { practiceEntity } = this.props;
    return (
      <Row>
        <Col md="12">
          <Button tag={Link} to={`/entity/synonym/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" /> <span className="d-none d-md-inline">Create a new Synonym</span>
          </Button>
          <h2>
            Practice [<b>{practiceEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="code">Code</span>
            </dt>
            <dd>{practiceEntity.code}</dd>
            <dt>
              <span id="description">Description</span>
            </dt>
            <dd>{practiceEntity.description}</dd>
            <br />
            <dt>
              <h3 id="synonyms">Synonyms</h3>
            </dt>
            <dd>
              <br />
              <div className="table-responsive">
                {practiceEntity.synonyms && practiceEntity.synonyms.length > 0 ? (
                  <Table responsive>
                    <thead>
                      <tr>
                        <th className="hand">Code</th>
                        <th className="hand">Description</th>
                        <th />
                      </tr>
                    </thead>
                    <tbody>
                      {practiceEntity.synonyms.map((synonym, i) => (
                        <tr key={`entity-${i}`}>
                          <td>
                            <a href={`/entity/synonym/${synonym.id}`}>{synonym.code}</a>
                          </td>
                          <td>{synonym.description}</td>
                          <td className="text-right">
                            <div className="btn-group flex-btn-group-container">
                              <a className="btn btn-info btn-sm" href={`/entity/synonym/${synonym.id}`}>
                                <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                              </a>
                              <Button tag={Link} to={`/entity/synonym/${synonym.id}/edit`} color="primary" size="sm">
                                <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                              </Button>
                              <a className="btn btn-danger btn-sm" href={`/entity/synonym/${synonym.id}/delete`}>
                                <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                              </a>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                ) : (
                  <div className="alert alert-warning">No Synonyms found</div>
                )}
              </div>
            </dd>
          </dl>
          <Button tag={Link} to="/entity/practice" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/practice/${practiceEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ practice }: IRootState) => ({
  practiceEntity: practice.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PracticeDetail);
