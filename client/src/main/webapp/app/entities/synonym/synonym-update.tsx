import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IPractice } from 'app/shared/model/practice.model';
import { getEntities as getPractices } from 'app/entities/practice/practice.reducer';
import { getEntity, updateEntity, createEntity, reset } from './synonym.reducer';
import { ISynonym } from 'app/shared/model/synonym.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ISynonymUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ISynonymUpdateState {
  isNew: boolean;
  practiceId: string;
}

export class SynonymUpdate extends React.Component<ISynonymUpdateProps, ISynonymUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      practiceId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPractices();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { synonymEntity } = this.props;
      const entity = {
        ...synonymEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/practice');
  };

  render() {
    const { synonymEntity, practices, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="enfokeProofOfConceptApp.synonym.home.createOrEditLabel">Create or edit a Synonym</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : synonymEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="synonym-id">ID</Label>
                    <AvInput id="synonym-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="codeLabel" for="synonym-code">
                    Code
                  </Label>
                  <AvField
                    id="synonym-code"
                    type="text"
                    name="code"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="descriptionLabel" for="synonym-description">
                    Description
                  </Label>
                  <AvField id="synonym-description" type="text" name="description" />
                </AvGroup>
                <AvGroup>
                  <Label for="synonym-practice">Practice</Label>
                  <AvInput id="synonym-practice" type="select" className="form-control" name="practice.id">
                    <option value="" key="0" />
                    {practices
                      ? practices.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.code} ({otherEntity.description})
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/practice" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  practices: storeState.practice.entities,
  synonymEntity: storeState.synonym.entity,
  loading: storeState.synonym.loading,
  updating: storeState.synonym.updating,
  updateSuccess: storeState.synonym.updateSuccess
});

const mapDispatchToProps = {
  getPractices,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SynonymUpdate);
