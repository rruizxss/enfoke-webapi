import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ISynonym, defaultValue } from 'app/shared/model/synonym.model';

export const ACTION_TYPES = {
  FETCH_SYNONYM_LIST: 'synonym/FETCH_SYNONYM_LIST',
  FETCH_SYNONYM: 'synonym/FETCH_SYNONYM',
  CREATE_SYNONYM: 'synonym/CREATE_SYNONYM',
  UPDATE_SYNONYM: 'synonym/UPDATE_SYNONYM',
  DELETE_SYNONYM: 'synonym/DELETE_SYNONYM',
  RESET: 'synonym/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ISynonym>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type SynonymState = Readonly<typeof initialState>;

// Reducer

export default (state: SynonymState = initialState, action): SynonymState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SYNONYM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SYNONYM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_SYNONYM):
    case REQUEST(ACTION_TYPES.UPDATE_SYNONYM):
    case REQUEST(ACTION_TYPES.DELETE_SYNONYM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_SYNONYM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SYNONYM):
    case FAILURE(ACTION_TYPES.CREATE_SYNONYM):
    case FAILURE(ACTION_TYPES.UPDATE_SYNONYM):
    case FAILURE(ACTION_TYPES.DELETE_SYNONYM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_SYNONYM_LIST):
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_SYNONYM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_SYNONYM):
    case SUCCESS(ACTION_TYPES.UPDATE_SYNONYM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_SYNONYM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/synonyms';

// Actions

export const getEntities: ICrudGetAllAction<ISynonym> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_SYNONYM_LIST,
    payload: axios.get<ISynonym>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ISynonym> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SYNONYM,
    payload: axios.get<ISynonym>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ISynonym> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SYNONYM,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<ISynonym> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SYNONYM,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ISynonym> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SYNONYM,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
