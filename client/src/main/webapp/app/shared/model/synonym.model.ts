import { IPractice } from 'app/shared/model/practice.model';

export interface ISynonym {
  id?: number;
  code?: string;
  description?: string;
  practice?: IPractice;
}

export const defaultValue: Readonly<ISynonym> = {};
