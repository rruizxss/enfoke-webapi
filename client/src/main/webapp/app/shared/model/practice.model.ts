import { ISynonym } from 'app/shared/model/synonym.model';

export interface IPractice {
  id?: number;
  code?: string;
  description?: string;
  synonyms?: ISynonym[];
}

export const defaultValue: Readonly<IPractice> = {};
