/**
 * View Models used by Spring MVC REST controllers.
 */
package com.xside.enfoke.web.rest.vm;
