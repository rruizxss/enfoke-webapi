package com.xside.enfoke.web.rest;

import com.xside.enfoke.domain.Synonym;
import com.xside.enfoke.service.SynonymService;
import com.xside.enfoke.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.xside.enfoke.domain.Synonym}.
 */
@RestController
@RequestMapping("/api")
public class SynonymResource {

    private final Logger log = LoggerFactory.getLogger(SynonymResource.class);

    private static final String ENTITY_NAME = "synonym";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SynonymService synonymService;

    public SynonymResource(SynonymService synonymService) {
        this.synonymService = synonymService;
    }

    /**
     * {@code POST  /synonyms} : Create a new synonym.
     *
     * @param synonym the synonym to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new synonym, or with status {@code 400 (Bad Request)} if the synonym has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/synonyms")
    public ResponseEntity<Synonym> createSynonym(@Valid @RequestBody Synonym synonym) throws URISyntaxException {
        log.debug("REST request to save Synonym : {}", synonym);
        if (synonym.getId() != null) {
            throw new BadRequestAlertException("A new synonym cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Synonym result = synonymService.save(synonym);
        return ResponseEntity.created(new URI("/api/synonyms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /synonyms} : Updates an existing synonym.
     *
     * @param synonym the synonym to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated synonym,
     * or with status {@code 400 (Bad Request)} if the synonym is not valid,
     * or with status {@code 500 (Internal Server Error)} if the synonym couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/synonyms")
    public ResponseEntity<Synonym> updateSynonym(@Valid @RequestBody Synonym synonym) throws URISyntaxException {
        log.debug("REST request to update Synonym : {}", synonym);
        if (synonym.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Synonym result = synonymService.save(synonym);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, synonym.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /synonyms} : get all the synonyms.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of synonyms in body.
     */
    @GetMapping("/synonyms")
    public ResponseEntity<List<Synonym>> getAllSynonyms(Pageable pageable) {
        log.debug("REST request to get a page of Synonyms");
        Page<Synonym> page = synonymService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /synonyms/:id} : get the "id" synonym.
     *
     * @param id the id of the synonym to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the synonym, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/synonyms/{id}")
    public ResponseEntity<Synonym> getSynonym(@PathVariable Long id) {
        log.debug("REST request to get Synonym : {}", id);
        Optional<Synonym> synonym = synonymService.findOne(id);
        return ResponseUtil.wrapOrNotFound(synonym);
    }

    /**
     * {@code DELETE  /synonyms/:id} : delete the "id" synonym.
     *
     * @param id the id of the synonym to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/synonyms/{id}")
    public ResponseEntity<Void> deleteSynonym(@PathVariable Long id) {
        log.debug("REST request to delete Synonym : {}", id);
        synonymService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
