package com.xside.enfoke.web.rest;

import com.xside.enfoke.domain.Practice;
import com.xside.enfoke.service.PracticeService;
import com.xside.enfoke.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.xside.enfoke.domain.Practice}.
 */
@RestController
@RequestMapping("/api")
public class PracticeResource {

    private final Logger log = LoggerFactory.getLogger(PracticeResource.class);

    private static final String ENTITY_NAME = "practice";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PracticeService practiceService;

    public PracticeResource(PracticeService practiceService) {
        this.practiceService = practiceService;
    }

    /**
     * {@code POST  /practices} : Create a new practice.
     *
     * @param practice the practice to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new practice, or with status {@code 400 (Bad Request)} if the practice has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/practices")
    public ResponseEntity<Practice> createPractice(@Valid @RequestBody Practice practice) throws URISyntaxException {
        log.debug("REST request to save Practice : {}", practice);
        if (practice.getId() != null) {
            throw new BadRequestAlertException("A new practice cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Practice result = practiceService.save(practice);
        return ResponseEntity.created(new URI("/api/practices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /practices} : Updates an existing practice.
     *
     * @param practice the practice to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated practice,
     * or with status {@code 400 (Bad Request)} if the practice is not valid,
     * or with status {@code 500 (Internal Server Error)} if the practice couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/practices")
    public ResponseEntity<Practice> updatePractice(@Valid @RequestBody Practice practice) throws URISyntaxException {
        log.debug("REST request to update Practice : {}", practice);
        if (practice.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Practice result = practiceService.save(practice);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, practice.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /practices} : get all the practices.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of practices in body.
     */
    @GetMapping("/practices")
    public ResponseEntity<List<Practice>> getAllPractices(Pageable pageable) {
        log.debug("REST request to get a page of Practices");
        Page<Practice> page = practiceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /practices/:id} : get the "id" practice.
     *
     * @param id the id of the practice to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the practice, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/practices/{id}")
    public ResponseEntity<Practice> getPractice(@PathVariable Long id) {
        log.debug("REST request to get Practice : {}", id);
        Optional<Practice> practice = practiceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(practice);
    }

    /**
     * {@code DELETE  /practices/:id} : delete the "id" practice.
     *
     * @param id the id of the practice to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/practices/{id}")
    public ResponseEntity<Void> deletePractice(@PathVariable Long id) {
        log.debug("REST request to delete Practice : {}", id);
        practiceService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
