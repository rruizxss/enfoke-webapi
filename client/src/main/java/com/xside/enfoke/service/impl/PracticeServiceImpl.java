package com.xside.enfoke.service.impl;

import com.xside.enfoke.service.PracticeService;
import com.xside.enfoke.domain.Practice;
import com.xside.enfoke.repository.PracticeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Practice}.
 */
@Service
@Transactional
public class PracticeServiceImpl implements PracticeService {

    private final Logger log = LoggerFactory.getLogger(PracticeServiceImpl.class);

    private final PracticeRepository practiceRepository;

    public PracticeServiceImpl(PracticeRepository practiceRepository) {
        this.practiceRepository = practiceRepository;
    }

    /**
     * Save a practice.
     *
     * @param practice the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Practice save(Practice practice) {
        log.debug("Request to save Practice : {}", practice);
        return practiceRepository.save(practice);
    }

    /**
     * Get all the practices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Practice> findAll(Pageable pageable) {
        log.debug("Request to get all Practices");
        return practiceRepository.findAll(pageable);
    }


    /**
     * Get one practice by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Practice> findOne(Long id) {
        log.debug("Request to get Practice : {}", id);
        return practiceRepository.findById(id);
    }

    /**
     * Delete the practice by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Practice : {}", id);
        practiceRepository.deleteById(id);
    }
}
