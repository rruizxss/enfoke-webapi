package com.xside.enfoke.service;

import com.xside.enfoke.domain.Practice;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Practice}.
 */
public interface PracticeService {

    /**
     * Save a practice.
     *
     * @param practice the entity to save.
     * @return the persisted entity.
     */
    Practice save(Practice practice);

    /**
     * Get all the practices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Practice> findAll(Pageable pageable);


    /**
     * Get the "id" practice.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Practice> findOne(Long id);

    /**
     * Delete the "id" practice.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
