package com.xside.enfoke.service;

import com.xside.enfoke.domain.Synonym;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Synonym}.
 */
public interface SynonymService {

    /**
     * Save a synonym.
     *
     * @param synonym the entity to save.
     * @return the persisted entity.
     */
    Synonym save(Synonym synonym);

    /**
     * Get all the synonyms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Synonym> findAll(Pageable pageable);


    /**
     * Get the "id" synonym.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Synonym> findOne(Long id);

    /**
     * Delete the "id" synonym.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
