package com.xside.enfoke.service.impl;

import com.xside.enfoke.service.SynonymService;
import com.xside.enfoke.domain.Synonym;
import com.xside.enfoke.repository.SynonymRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Synonym}.
 */
@Service
@Transactional
public class SynonymServiceImpl implements SynonymService {

    private final Logger log = LoggerFactory.getLogger(SynonymServiceImpl.class);

    private final SynonymRepository synonymRepository;

    public SynonymServiceImpl(SynonymRepository synonymRepository) {
        this.synonymRepository = synonymRepository;
    }

    /**
     * Save a synonym.
     *
     * @param synonym the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Synonym save(Synonym synonym) {
        log.debug("Request to save Synonym : {}", synonym);
        return synonymRepository.save(synonym);
    }

    /**
     * Get all the synonyms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Synonym> findAll(Pageable pageable) {
        log.debug("Request to get all Synonyms");
        return synonymRepository.findAll(pageable);
    }


    /**
     * Get one synonym by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Synonym> findOne(Long id) {
        log.debug("Request to get Synonym : {}", id);
        return synonymRepository.findById(id);
    }

    /**
     * Delete the synonym by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Synonym : {}", id);
        synonymRepository.deleteById(id);
    }
}
