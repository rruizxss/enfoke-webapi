package com.xside.enfoke.repository;

import com.xside.enfoke.domain.Synonym;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Synonym entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SynonymRepository extends JpaRepository<Synonym, Long> {

}
