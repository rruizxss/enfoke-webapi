package com.xside.enfoke.repository;

import com.xside.enfoke.domain.Practice;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Practice entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PracticeRepository extends JpaRepository<Practice, Long> {

}
