package com.xside.enfoke.web.rest;

import com.xside.enfoke.EnfokeProofOfConceptApp;
import com.xside.enfoke.domain.Synonym;
import com.xside.enfoke.repository.SynonymRepository;
import com.xside.enfoke.service.SynonymService;
import com.xside.enfoke.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.xside.enfoke.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SynonymResource} REST controller.
 */
@SpringBootTest(classes = EnfokeProofOfConceptApp.class)
public class SynonymResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private SynonymRepository synonymRepository;

    @Autowired
    private SynonymService synonymService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSynonymMockMvc;

    private Synonym synonym;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SynonymResource synonymResource = new SynonymResource(synonymService);
        this.restSynonymMockMvc = MockMvcBuilders.standaloneSetup(synonymResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Synonym createEntity(EntityManager em) {
        Synonym synonym = new Synonym()
            .code(DEFAULT_CODE)
            .description(DEFAULT_DESCRIPTION);
        return synonym;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Synonym createUpdatedEntity(EntityManager em) {
        Synonym synonym = new Synonym()
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION);
        return synonym;
    }

    @BeforeEach
    public void initTest() {
        synonym = createEntity(em);
    }

    @Test
    @Transactional
    public void createSynonym() throws Exception {
        int databaseSizeBeforeCreate = synonymRepository.findAll().size();

        // Create the Synonym
        restSynonymMockMvc.perform(post("/api/synonyms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(synonym)))
            .andExpect(status().isCreated());

        // Validate the Synonym in the database
        List<Synonym> synonymList = synonymRepository.findAll();
        assertThat(synonymList).hasSize(databaseSizeBeforeCreate + 1);
        Synonym testSynonym = synonymList.get(synonymList.size() - 1);
        assertThat(testSynonym.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testSynonym.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createSynonymWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = synonymRepository.findAll().size();

        // Create the Synonym with an existing ID
        synonym.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSynonymMockMvc.perform(post("/api/synonyms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(synonym)))
            .andExpect(status().isBadRequest());

        // Validate the Synonym in the database
        List<Synonym> synonymList = synonymRepository.findAll();
        assertThat(synonymList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = synonymRepository.findAll().size();
        // set the field null
        synonym.setCode(null);

        // Create the Synonym, which fails.

        restSynonymMockMvc.perform(post("/api/synonyms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(synonym)))
            .andExpect(status().isBadRequest());

        List<Synonym> synonymList = synonymRepository.findAll();
        assertThat(synonymList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSynonyms() throws Exception {
        // Initialize the database
        synonymRepository.saveAndFlush(synonym);

        // Get all the synonymList
        restSynonymMockMvc.perform(get("/api/synonyms?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(synonym.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @Test
    @Transactional
    public void getSynonym() throws Exception {
        // Initialize the database
        synonymRepository.saveAndFlush(synonym);

        // Get the synonym
        restSynonymMockMvc.perform(get("/api/synonyms/{id}", synonym.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(synonym.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSynonym() throws Exception {
        // Get the synonym
        restSynonymMockMvc.perform(get("/api/synonyms/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSynonym() throws Exception {
        // Initialize the database
        synonymService.save(synonym);

        int databaseSizeBeforeUpdate = synonymRepository.findAll().size();

        // Update the synonym
        Synonym updatedSynonym = synonymRepository.findById(synonym.getId()).get();
        // Disconnect from session so that the updates on updatedSynonym are not directly saved in db
        em.detach(updatedSynonym);
        updatedSynonym
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION);

        restSynonymMockMvc.perform(put("/api/synonyms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSynonym)))
            .andExpect(status().isOk());

        // Validate the Synonym in the database
        List<Synonym> synonymList = synonymRepository.findAll();
        assertThat(synonymList).hasSize(databaseSizeBeforeUpdate);
        Synonym testSynonym = synonymList.get(synonymList.size() - 1);
        assertThat(testSynonym.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testSynonym.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingSynonym() throws Exception {
        int databaseSizeBeforeUpdate = synonymRepository.findAll().size();

        // Create the Synonym

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSynonymMockMvc.perform(put("/api/synonyms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(synonym)))
            .andExpect(status().isBadRequest());

        // Validate the Synonym in the database
        List<Synonym> synonymList = synonymRepository.findAll();
        assertThat(synonymList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSynonym() throws Exception {
        // Initialize the database
        synonymService.save(synonym);

        int databaseSizeBeforeDelete = synonymRepository.findAll().size();

        // Delete the synonym
        restSynonymMockMvc.perform(delete("/api/synonyms/{id}", synonym.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Synonym> synonymList = synonymRepository.findAll();
        assertThat(synonymList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Synonym.class);
        Synonym synonym1 = new Synonym();
        synonym1.setId(1L);
        Synonym synonym2 = new Synonym();
        synonym2.setId(synonym1.getId());
        assertThat(synonym1).isEqualTo(synonym2);
        synonym2.setId(2L);
        assertThat(synonym1).isNotEqualTo(synonym2);
        synonym1.setId(null);
        assertThat(synonym1).isNotEqualTo(synonym2);
    }
}
