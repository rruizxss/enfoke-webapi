package com.xside.enfoke.web.rest;

import com.xside.enfoke.EnfokeProofOfConceptApp;
import com.xside.enfoke.domain.Practice;
import com.xside.enfoke.repository.PracticeRepository;
import com.xside.enfoke.service.PracticeService;
import com.xside.enfoke.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.xside.enfoke.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PracticeResource} REST controller.
 */
@SpringBootTest(classes = EnfokeProofOfConceptApp.class)
public class PracticeResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private PracticeRepository practiceRepository;

    @Autowired
    private PracticeService practiceService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPracticeMockMvc;

    private Practice practice;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PracticeResource practiceResource = new PracticeResource(practiceService);
        this.restPracticeMockMvc = MockMvcBuilders.standaloneSetup(practiceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Practice createEntity(EntityManager em) {
        Practice practice = new Practice()
            .code(DEFAULT_CODE)
            .description(DEFAULT_DESCRIPTION);
        return practice;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Practice createUpdatedEntity(EntityManager em) {
        Practice practice = new Practice()
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION);
        return practice;
    }

    @BeforeEach
    public void initTest() {
        practice = createEntity(em);
    }

    @Test
    @Transactional
    public void createPractice() throws Exception {
        int databaseSizeBeforeCreate = practiceRepository.findAll().size();

        // Create the Practice
        restPracticeMockMvc.perform(post("/api/practices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(practice)))
            .andExpect(status().isCreated());

        // Validate the Practice in the database
        List<Practice> practiceList = practiceRepository.findAll();
        assertThat(practiceList).hasSize(databaseSizeBeforeCreate + 1);
        Practice testPractice = practiceList.get(practiceList.size() - 1);
        assertThat(testPractice.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testPractice.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createPracticeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = practiceRepository.findAll().size();

        // Create the Practice with an existing ID
        practice.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPracticeMockMvc.perform(post("/api/practices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(practice)))
            .andExpect(status().isBadRequest());

        // Validate the Practice in the database
        List<Practice> practiceList = practiceRepository.findAll();
        assertThat(practiceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = practiceRepository.findAll().size();
        // set the field null
        practice.setCode(null);

        // Create the Practice, which fails.

        restPracticeMockMvc.perform(post("/api/practices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(practice)))
            .andExpect(status().isBadRequest());

        List<Practice> practiceList = practiceRepository.findAll();
        assertThat(practiceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPractices() throws Exception {
        // Initialize the database
        practiceRepository.saveAndFlush(practice);

        // Get all the practiceList
        restPracticeMockMvc.perform(get("/api/practices?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(practice.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @Test
    @Transactional
    public void getPractice() throws Exception {
        // Initialize the database
        practiceRepository.saveAndFlush(practice);

        // Get the practice
        restPracticeMockMvc.perform(get("/api/practices/{id}", practice.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(practice.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPractice() throws Exception {
        // Get the practice
        restPracticeMockMvc.perform(get("/api/practices/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePractice() throws Exception {
        // Initialize the database
        practiceService.save(practice);

        int databaseSizeBeforeUpdate = practiceRepository.findAll().size();

        // Update the practice
        Practice updatedPractice = practiceRepository.findById(practice.getId()).get();
        // Disconnect from session so that the updates on updatedPractice are not directly saved in db
        em.detach(updatedPractice);
        updatedPractice
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION);

        restPracticeMockMvc.perform(put("/api/practices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPractice)))
            .andExpect(status().isOk());

        // Validate the Practice in the database
        List<Practice> practiceList = practiceRepository.findAll();
        assertThat(practiceList).hasSize(databaseSizeBeforeUpdate);
        Practice testPractice = practiceList.get(practiceList.size() - 1);
        assertThat(testPractice.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testPractice.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingPractice() throws Exception {
        int databaseSizeBeforeUpdate = practiceRepository.findAll().size();

        // Create the Practice

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPracticeMockMvc.perform(put("/api/practices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(practice)))
            .andExpect(status().isBadRequest());

        // Validate the Practice in the database
        List<Practice> practiceList = practiceRepository.findAll();
        assertThat(practiceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePractice() throws Exception {
        // Initialize the database
        practiceService.save(practice);

        int databaseSizeBeforeDelete = practiceRepository.findAll().size();

        // Delete the practice
        restPracticeMockMvc.perform(delete("/api/practices/{id}", practice.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Practice> practiceList = practiceRepository.findAll();
        assertThat(practiceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Practice.class);
        Practice practice1 = new Practice();
        practice1.setId(1L);
        Practice practice2 = new Practice();
        practice2.setId(practice1.getId());
        assertThat(practice1).isEqualTo(practice2);
        practice2.setId(2L);
        assertThat(practice1).isNotEqualTo(practice2);
        practice1.setId(null);
        assertThat(practice1).isNotEqualTo(practice2);
    }
}
