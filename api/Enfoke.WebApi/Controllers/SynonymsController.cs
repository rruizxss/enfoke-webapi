﻿using System;
using System.Threading.Tasks;
using Enfoke.Domain.Services.Interfaces;
using Enfoke.Infraestructure.Models;
using Microsoft.AspNetCore.Mvc;

namespace Enfoke.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PracticeAliasController : ControllerBase
    {
        private readonly IPracticeAliasService _service;
        
        public PracticeAliasController(IPracticeAliasService service)
        {
            _service = service;
        }
        
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var practiceAliases = await _service.Get();

            return Ok(practiceAliases);
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            try
            {
                var practiceAlias = await _service.Get(id);

                return Ok(practiceAlias);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] PracticeAlias practiceAlias)
        {
            try
            {
                await _service.Post(practiceAlias);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] PracticeAlias practiceAlias)
        {
            try
            {
                await _service.Put(practiceAlias);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _service.Delete(id);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
