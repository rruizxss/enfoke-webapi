﻿using System;
using System.Threading.Tasks;
using Enfoke.Domain.Services.Interfaces;
using Enfoke.Infraestructure.Models;
using Microsoft.AspNetCore.Mvc;

namespace Enfoke.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PracticesController : ControllerBase
    {
        private readonly IPracticeService _service;
        
        public PracticesController(IPracticeService service)
        {
            _service = service;
        }
        
        [HttpGet]
        public async Task<ActionResult> Get([FromQuery] bool withAliases = false)
        {
            var practices = await _service.Get(withAliases);

            return Ok(practices);
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            try
            {
                var practice = await _service.Get(id);

                return Ok(practice);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Practice practice)
        {
            try
            {
                await _service.Post(practice);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] Practice practice)
        {
            try
            {
                await _service.Put(practice);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _service.Delete(id);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
