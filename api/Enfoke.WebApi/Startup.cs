﻿using Enfoke.Domain.Services;
using Enfoke.Domain.Services.Interfaces;
using Enfoke.Infraestructure.Repositories;
using Enfoke.Infraestructure.Repositories.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Enfoke.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IPracticeRepository, PracticeRepository>();
            services.AddScoped<IPracticeAliasRepository, PracticeAliasRepository>();
            
            services.AddTransient<IPracticeService, PracticeService>();
            services.AddTransient<IPracticeAliasService, PracticeAliasService>();
            
            services.AddCors(options =>
            {
                options.AddPolicy("_corsOrigins", builder =>
                {
                    builder.AllowAnyMethod().AllowAnyOrigin().AllowAnyHeader();
                });
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);;

            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Title = "Enfoke";
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();

            app.UseSwaggerUi3();

            app.UseMvc();
        }
    }
}
