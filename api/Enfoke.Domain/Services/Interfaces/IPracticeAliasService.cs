﻿using Enfoke.Infraestructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Enfoke.Domain.Services.Interfaces
{
    public interface IPracticeAliasService
    {
        Task<List<PracticeAlias>> Get();

        Task<PracticeAlias> Get(int id);

        Task Post(PracticeAlias practice);

        Task Put(PracticeAlias practice);

        Task Delete(int id);
    }
}
