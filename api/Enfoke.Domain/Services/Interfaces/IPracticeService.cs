﻿using Enfoke.Infraestructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Enfoke.Domain.Services.Interfaces
{
    public interface IPracticeService
    {
        Task<List<Practice>> Get(bool withAliases);

        Task<Practice> Get(int id);

        Task Post(Practice practice);

        Task Put(Practice practice);

        Task Delete(int id);
    }
}
