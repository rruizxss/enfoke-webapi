﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Enfoke.Domain.Services.Interfaces;
using Enfoke.Infraestructure.Models;
using Enfoke.Infraestructure.Repositories.Interfaces;

namespace Enfoke.Domain.Services
{
    public class PracticeService : IPracticeService
    {
        private IPracticeRepository _repository { get; set; }
        
        public PracticeService(IPracticeRepository repository)
        {
            _repository = repository;
        }

        public Task<List<Practice>> Get(bool withAliases)
        {
            IEnumerable<Practice> practices;

            if (withAliases)
            {
                practices = _repository.GetAll();
            }
            else
            {
                practices = _repository.GetAllWithAliases();
            }

            return Task.FromResult(practices.ToList());
        }

        public Task<Practice> Get(int id)
        {
            var practice = _repository.GetBy(id);

            return Task.FromResult(practice);
        }

        public Task Post(Practice practice)
        {
            _repository.Update(practice);

            return Task.FromResult(true);
        }

        public Task Put(Practice practice)
        {
            _repository.Put(practice);

            return Task.FromResult(true);
        }

        public Task Delete(int id)
        {
            _repository.Delete(id);

            return Task.FromResult(true);
        }
    }
}
