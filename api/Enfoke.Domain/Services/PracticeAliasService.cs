﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Enfoke.Domain.Services.Interfaces;
using Enfoke.Infraestructure.Models;
using Enfoke.Infraestructure.Repositories.Interfaces;

namespace Enfoke.Domain.Services
{
    public class PracticeAliasService : IPracticeAliasService
    {
        private IPracticeAliasRepository _repository { get; set; }
        
        public PracticeAliasService(IPracticeAliasRepository repository)
        {
            _repository = repository;
        }

        public Task<List<PracticeAlias>> Get()
        {
            var practiceAliases = _repository.GetAll();
            
            return Task.FromResult(practiceAliases.ToList());
        }

        public Task<PracticeAlias> Get(int id)
        {
            var practiceAlias = _repository.GetBy(id);

            return Task.FromResult(practiceAlias);
        }

        public Task Post(PracticeAlias practiceAlias)
        {
            _repository.Update(practiceAlias);

            return Task.FromResult(true);
        }

        public Task Put(PracticeAlias practiceAlias)
        {
            _repository.Put(practiceAlias);

            return Task.FromResult(true);
        }

        public Task Delete(int id)
        {
            _repository.Delete(id);

            return Task.FromResult(true);
        }
    }
}
