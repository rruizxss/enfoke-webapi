﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class City
    {
        public City()
        {
            HealthcareCenter = new HashSet<HealthcareCenter>();
            Patient = new HashSet<Patient>();
        }

        public int CitId { get; set; }
        public string CitName { get; set; }
        public int? CitStateId { get; set; }

        public virtual State CitState { get; set; }
        public virtual ICollection<HealthcareCenter> HealthcareCenter { get; set; }
        public virtual ICollection<Patient> Patient { get; set; }
    }
}
