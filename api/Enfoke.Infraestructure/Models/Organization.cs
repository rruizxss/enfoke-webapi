﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Organization
    {
        public Organization()
        {
            Connection = new HashSet<Connection>();
            HealthcareCenter = new HashSet<HealthcareCenter>();
            InverseOrgParent = new HashSet<Organization>();
            PracticeOrganization = new HashSet<PracticeOrganization>();
            PreTranslationLog = new HashSet<PreTranslationLog>();
            PreTranslationRule = new HashSet<PreTranslationRule>();
        }

        public int OrgId { get; set; }
        public string OrgName { get; set; }
        public byte OrgLevel { get; set; }
        public int? OrgParentId { get; set; }
        public DateTime OrgCreateDate { get; set; }
        public DateTime? OrgUpdateDate { get; set; }
        public DateTime? OrgDeleteDate { get; set; }
        public int OrgCreateUserId { get; set; }
        public int? OrgUpdateUserId { get; set; }
        public int? OrgDeleteUserId { get; set; }

        public virtual Organization OrgParent { get; set; }
        public virtual ICollection<Connection> Connection { get; set; }
        public virtual ICollection<HealthcareCenter> HealthcareCenter { get; set; }
        public virtual ICollection<Organization> InverseOrgParent { get; set; }
        public virtual ICollection<PracticeOrganization> PracticeOrganization { get; set; }
        public virtual ICollection<PreTranslationLog> PreTranslationLog { get; set; }
        public virtual ICollection<PreTranslationRule> PreTranslationRule { get; set; }
    }
}
