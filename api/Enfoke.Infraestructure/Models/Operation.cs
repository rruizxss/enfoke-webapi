﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Operation
    {
        public int OpeId { get; set; }
        public string OpeCode { get; set; }
        public string OpeName { get; set; }
        public int? OpeCaptchaAttempts { get; set; }
        public int? OpeCaptchaAttemptsSeconds { get; set; }
        public int? OpeLockAttempts { get; set; }
        public int? OpeLockAttemptsSeconds { get; set; }
    }
}
