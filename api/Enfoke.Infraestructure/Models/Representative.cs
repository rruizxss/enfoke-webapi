﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Representative
    {
        public Representative()
        {
            RepresentativeHealthCareCenter = new HashSet<RepresentativeHealthCareCenter>();
            RepresentativeInsurance = new HashSet<RepresentativeInsurance>();
            RepresentativeService = new HashSet<RepresentativeService>();
        }

        public int RepId { get; set; }
        public int RepPatientId { get; set; }
        public string RepCorporativeEmail { get; set; }

        public virtual Patient RepPatient { get; set; }
        public virtual ICollection<RepresentativeHealthCareCenter> RepresentativeHealthCareCenter { get; set; }
        public virtual ICollection<RepresentativeInsurance> RepresentativeInsurance { get; set; }
        public virtual ICollection<RepresentativeService> RepresentativeService { get; set; }
    }
}
