﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class SecurityActions
    {
        public SecurityActions()
        {
            SecurityProfilesActions = new HashSet<SecurityProfilesActions>();
        }

        public int ActId { get; set; }
        public string ActName { get; set; }
        public string ActDescription { get; set; }
        public int ActMenId { get; set; }

        public virtual SecurityMenus ActMen { get; set; }
        public virtual ICollection<SecurityProfilesActions> SecurityProfilesActions { get; set; }
    }
}
