﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Enfoke.Infraestructure.Models
{
    public partial class MiSalud_27_MobileContext : DbContext
    {
        public MiSalud_27_MobileContext()
        {
        }

        public MiSalud_27_MobileContext(DbContextOptions<MiSalud_27_MobileContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ActivationToken> ActivationToken { get; set; }
        public virtual DbSet<Appointment> Appointment { get; set; }
        public virtual DbSet<AppointmentPhoto> AppointmentPhoto { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<ClientApp> ClientApp { get; set; }
        public virtual DbSet<Connection> Connection { get; set; }
        public virtual DbSet<ConnectionLog> ConnectionLog { get; set; }
        public virtual DbSet<ConnectionLogType> ConnectionLogType { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<DateOffset> DateOffset { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<EnrollmentType> EnrollmentType { get; set; }
        public virtual DbSet<Filter> Filter { get; set; }
        public virtual DbSet<ForcedCombos> ForcedCombos { get; set; }
        public virtual DbSet<Gender> Gender { get; set; }
        public virtual DbSet<HealthcareCenter> HealthcareCenter { get; set; }
        public virtual DbSet<IdentificationType> IdentificationType { get; set; }
        public virtual DbSet<Info> Info { get; set; }
        public virtual DbSet<InfoSettings> InfoSettings { get; set; }
        public virtual DbSet<Insurance> Insurance { get; set; }
        public virtual DbSet<InsuranceAlias> InsuranceAlias { get; set; }
        public virtual DbSet<InsurancePlan> InsurancePlan { get; set; }
        public virtual DbSet<InsurancePlanPatient> InsurancePlanPatient { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<MessageMobile> MessageMobile { get; set; }
        public virtual DbSet<MessagePracticeGroup> MessagePracticeGroup { get; set; }
        public virtual DbSet<MobileLog> MobileLog { get; set; }
        public virtual DbSet<MobileService> MobileService { get; set; }
        public virtual DbSet<MobileSession> MobileSession { get; set; }
        public virtual DbSet<OffersDistribution> OffersDistribution { get; set; }
        public virtual DbSet<Operation> Operation { get; set; }
        public virtual DbSet<OperationLog> OperationLog { get; set; }
        public virtual DbSet<Operator> Operator { get; set; }
        public virtual DbSet<Organization> Organization { get; set; }
        public virtual DbSet<Patient> Patient { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<Physician> Physician { get; set; }
        public virtual DbSet<Practice> Practice { get; set; }
        public virtual DbSet<PracticeAlias> PracticeAlias { get; set; }
        public virtual DbSet<PracticeFromPreTranslationRule> PracticeFromPreTranslationRule { get; set; }
        public virtual DbSet<PracticeGroup> PracticeGroup { get; set; }
        public virtual DbSet<PracticeGroupPractice> PracticeGroupPractice { get; set; }
        public virtual DbSet<PracticeOrganization> PracticeOrganization { get; set; }
        public virtual DbSet<PracticeResponseQuestionPracticeGroup> PracticeResponseQuestionPracticeGroup { get; set; }
        public virtual DbSet<PracticeRestriction> PracticeRestriction { get; set; }
        public virtual DbSet<PracticeRestrictionType> PracticeRestrictionType { get; set; }
        public virtual DbSet<PracticeToPreTranslationRule> PracticeToPreTranslationRule { get; set; }
        public virtual DbSet<PreTranslationLog> PreTranslationLog { get; set; }
        public virtual DbSet<PreTranslationLogDetail> PreTranslationLogDetail { get; set; }
        public virtual DbSet<PreTranslationRule> PreTranslationRule { get; set; }
        public virtual DbSet<Question> Question { get; set; }
        public virtual DbSet<QuestionPracticeGroup> QuestionPracticeGroup { get; set; }
        public virtual DbSet<RelationshipType> RelationshipType { get; set; }
        public virtual DbSet<ReportStatus> ReportStatus { get; set; }
        public virtual DbSet<Representative> Representative { get; set; }
        public virtual DbSet<RepresentativeHealthCareCenter> RepresentativeHealthCareCenter { get; set; }
        public virtual DbSet<RepresentativeInsurance> RepresentativeInsurance { get; set; }
        public virtual DbSet<RepresentativeService> RepresentativeService { get; set; }
        public virtual DbSet<ReservationType> ReservationType { get; set; }
        public virtual DbSet<ResponseQuestionPracticeGroup> ResponseQuestionPracticeGroup { get; set; }
        public virtual DbSet<RuleType> RuleType { get; set; }
        public virtual DbSet<Script> Script { get; set; }
        public virtual DbSet<SecurityActions> SecurityActions { get; set; }
        public virtual DbSet<SecurityMenus> SecurityMenus { get; set; }
        public virtual DbSet<SecurityProfiles> SecurityProfiles { get; set; }
        public virtual DbSet<SecurityProfilesActions> SecurityProfilesActions { get; set; }
        public virtual DbSet<SecurityProfilesUsers> SecurityProfilesUsers { get; set; }
        public virtual DbSet<SecurityUsers> SecurityUsers { get; set; }
        public virtual DbSet<Service> Service { get; set; }
        public virtual DbSet<SourceDistribution> SourceDistribution { get; set; }
        public virtual DbSet<State> State { get; set; }
        public virtual DbSet<TimeRange> TimeRange { get; set; }
        public virtual DbSet<Translation> Translation { get; set; }
        public virtual DbSet<TranslationField> TranslationField { get; set; }
        public virtual DbSet<TranslationLog> TranslationLog { get; set; }
        public virtual DbSet<TranslationLogType> TranslationLogType { get; set; }
        public virtual DbSet<TranslationType> TranslationType { get; set; }
        public virtual DbSet<TranslationValues> TranslationValues { get; set; }
        public virtual DbSet<UserOrganization> UserOrganization { get; set; }
        public virtual DbSet<WebServiceType> WebServiceType { get; set; }

        // Unable to generate entity type for table 'dbo._T_IPP'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Log4Net'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.patient_user'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=ARGO\\SQLSERVER_DEV;Database=MiSalud_2.7_Mobile;User Id=devteam;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<ActivationToken>(entity =>
            {
                entity.HasKey(e => e.ActId);

                entity.HasIndex(e => new { e.ActUserId, e.ActToken })
                    .HasName("IX_ActivationToken")
                    .IsUnique();

                entity.Property(e => e.ActId).HasColumnName("act_id");

                entity.Property(e => e.ActCreateDate)
                    .HasColumnName("act_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ActCreateUserId).HasColumnName("act_create_user_id");

                entity.Property(e => e.ActDeleteDate)
                    .HasColumnName("act_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ActDeleteUserId).HasColumnName("act_delete_user_id");

                entity.Property(e => e.ActDiscriminator)
                    .HasColumnName("act_discriminator")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ActDueDate)
                    .HasColumnName("act_due_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ActToken)
                    .HasColumnName("act_token")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ActUpdateDate)
                    .HasColumnName("act_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ActUpdateUserId).HasColumnName("act_update_user_id");

                entity.Property(e => e.ActUserId).HasColumnName("act_user_id");

                entity.HasOne(d => d.ActUser)
                    .WithMany(p => p.ActivationToken)
                    .HasForeignKey(d => d.ActUserId)
                    .HasConstraintName("FK_ActivationToken_security_users");
            });

            modelBuilder.Entity<Appointment>(entity =>
            {
                entity.HasKey(e => e.AppId);

                entity.HasIndex(e => new { e.AppConnectionId, e.AppExternalId })
                    .HasName("IX_Appointment_1")
                    .IsUnique();

                entity.Property(e => e.AppId).HasColumnName("app_id");

                entity.Property(e => e.AppConnectionId).HasColumnName("app_connection_id");

                entity.Property(e => e.AppCreateDate)
                    .HasColumnName("app_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.AppCreateUserId).HasColumnName("app_create_user_id");

                entity.Property(e => e.AppExternalComboId).HasColumnName("app_external_combo_id");

                entity.Property(e => e.AppExternalId)
                    .IsRequired()
                    .HasColumnName("app_external_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AppCreateUser)
                    .WithMany(p => p.Appointment)
                    .HasForeignKey(d => d.AppCreateUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Appointment_Security_Users");
            });

            modelBuilder.Entity<AppointmentPhoto>(entity =>
            {
                entity.HasKey(e => e.AphId);

                entity.ToTable("Appointment_Photo");

                entity.Property(e => e.AphId).HasColumnName("aph_id");

                entity.Property(e => e.AphAppointmentExternalId).HasColumnName("aph_appointment_external_id");

                entity.Property(e => e.AphPhotoRaw)
                    .IsRequired()
                    .HasColumnName("aph_photo_raw");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.HasKey(e => e.CitId);

                entity.HasIndex(e => new { e.CitStateId, e.CitName })
                    .HasName("IX_City")
                    .IsUnique();

                entity.Property(e => e.CitId).HasColumnName("cit_id");

                entity.Property(e => e.CitName)
                    .HasColumnName("cit_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CitStateId).HasColumnName("cit_state_id");

                entity.HasOne(d => d.CitState)
                    .WithMany(p => p.City)
                    .HasForeignKey(d => d.CitStateId)
                    .HasConstraintName("FK_City_State");
            });

            modelBuilder.Entity<ClientApp>(entity =>
            {
                entity.HasKey(e => e.ClaId);

                entity.ToTable("client_app");

                entity.HasIndex(e => e.ClaName)
                    .HasName("IX_client_app")
                    .IsUnique();

                entity.Property(e => e.ClaId).HasColumnName("cla_id");

                entity.Property(e => e.ClaName)
                    .IsRequired()
                    .HasColumnName("cla_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Connection>(entity =>
            {
                entity.HasKey(e => e.ConId);

                entity.ToTable("connection");

                entity.HasIndex(e => new { e.ConWebServiceTypeId, e.ConOrganizationId, e.ConHealthcareCenterId })
                    .HasName("IX_web_service_type_id")
                    .IsUnique();

                entity.Property(e => e.ConId).HasColumnName("con_id");

                entity.Property(e => e.ConApplicationUrl)
                    .HasColumnName("con_application_url")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ConClientAppId).HasColumnName("con_client_app_id");

                entity.Property(e => e.ConDepartmentId).HasColumnName("con_department_id");

                entity.Property(e => e.ConExternalAccessUrl)
                    .HasColumnName("con_external_access_url")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ConHealthcareCenterId).HasColumnName("con_healthcare_center_id");

                entity.Property(e => e.ConMaxoffers)
                    .HasColumnName("con_maxoffers")
                    .HasDefaultValueSql("((500))");

                entity.Property(e => e.ConOrganizationId).HasColumnName("con_organization_id");

                entity.Property(e => e.ConPayloadInformation)
                    .HasColumnName("con_payload_information")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ConValidationUrl)
                    .HasColumnName("con_validation_url")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ConWebServiceTypeId).HasColumnName("con_web_service_type_id");

                entity.HasOne(d => d.ConClientApp)
                    .WithMany(p => p.Connection)
                    .HasForeignKey(d => d.ConClientAppId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_connection_client_app");

                entity.HasOne(d => d.ConDepartment)
                    .WithMany(p => p.Connection)
                    .HasForeignKey(d => d.ConDepartmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_connection_department");

                entity.HasOne(d => d.ConHealthcareCenter)
                    .WithMany(p => p.Connection)
                    .HasForeignKey(d => d.ConHealthcareCenterId)
                    .HasConstraintName("FK_connection_healthcare_center");

                entity.HasOne(d => d.ConOrganization)
                    .WithMany(p => p.Connection)
                    .HasForeignKey(d => d.ConOrganizationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_connection_organization");

                entity.HasOne(d => d.ConWebServiceType)
                    .WithMany(p => p.Connection)
                    .HasForeignKey(d => d.ConWebServiceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_connection_web_service_type");
            });

            modelBuilder.Entity<ConnectionLog>(entity =>
            {
                entity.HasKey(e => e.ColId);

                entity.ToTable("connection_log");

                entity.Property(e => e.ColId).HasColumnName("col_id");

                entity.Property(e => e.ColConnectionId).HasColumnName("col_connection_id");

                entity.Property(e => e.ColConnectionLogType).HasColumnName("col_connection_log_type");

                entity.Property(e => e.ColCreateDate)
                    .HasColumnName("col_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ColCreateUserId).HasColumnName("col_create_user_id");

                entity.Property(e => e.ColDateLog)
                    .HasColumnName("col_date_log")
                    .HasColumnType("datetime");

                entity.Property(e => e.ColDeleteDate)
                    .HasColumnName("col_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ColDeleteUserId).HasColumnName("col_delete_user_id");

                entity.Property(e => e.ColOperationLogId).HasColumnName("col_operation_log_id");

                entity.Property(e => e.ColUpdateDate)
                    .HasColumnName("col_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ColUpdateUserId).HasColumnName("col_update_user_id");

                entity.Property(e => e.ColWsResult)
                    .IsRequired()
                    .HasColumnName("col_ws_result")
                    .HasMaxLength(6000)
                    .IsUnicode(false);

                entity.Property(e => e.ColWsUrl)
                    .IsRequired()
                    .HasColumnName("col_ws_url")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.HasOne(d => d.ColConnection)
                    .WithMany(p => p.ConnectionLog)
                    .HasForeignKey(d => d.ColConnectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_connection_log_connection");

                entity.HasOne(d => d.ColConnectionLogTypeNavigation)
                    .WithMany(p => p.ConnectionLog)
                    .HasForeignKey(d => d.ColConnectionLogType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_connection_log_type");

                entity.HasOne(d => d.ColOperationLog)
                    .WithMany(p => p.ConnectionLog)
                    .HasForeignKey(d => d.ColOperationLogId)
                    .HasConstraintName("FK_connection_log_operation_log");
            });

            modelBuilder.Entity<ConnectionLogType>(entity =>
            {
                entity.HasKey(e => e.CltId);

                entity.ToTable("connection_log_type");

                entity.HasIndex(e => e.CltName)
                    .HasName("clt_by_name")
                    .IsUnique();

                entity.Property(e => e.CltId).HasColumnName("clt_id");

                entity.Property(e => e.CltName)
                    .IsRequired()
                    .HasColumnName("clt_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.HasKey(e => e.CouId);

                entity.HasIndex(e => e.CouName)
                    .HasName("IX_Country")
                    .IsUnique();

                entity.Property(e => e.CouId).HasColumnName("cou_id");

                entity.Property(e => e.CouName)
                    .HasColumnName("cou_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DateOffset>(entity =>
            {
                entity.HasKey(e => e.DaoId);

                entity.ToTable("date_offset");

                entity.HasIndex(e => new { e.DaoType, e.DaoName, e.DaoDeleteDate })
                    .HasName("IX_date_offset")
                    .IsUnique();

                entity.Property(e => e.DaoId).HasColumnName("dao_id");

                entity.Property(e => e.DaoCreateDate)
                    .HasColumnName("dao_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DaoCreateUserId).HasColumnName("dao_create_user_id");

                entity.Property(e => e.DaoDefault).HasColumnName("dao_default");

                entity.Property(e => e.DaoDeleteDate)
                    .HasColumnName("dao_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DaoDeleteUserId).HasColumnName("dao_delete_user_id");

                entity.Property(e => e.DaoName)
                    .IsRequired()
                    .HasColumnName("dao_name")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DaoOffsetFrom)
                    .HasColumnName("dao_offset_from")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DaoOffsetTo).HasColumnName("dao_offset_to");

                entity.Property(e => e.DaoType)
                    .IsRequired()
                    .HasColumnName("dao_type")
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaoUpdateDate)
                    .HasColumnName("dao_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DaoUpdateUserId).HasColumnName("dao_update_user_id");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.HasKey(e => e.DepId);

                entity.ToTable("department");

                entity.HasIndex(e => e.DepName)
                    .HasName("IX_department")
                    .IsUnique();

                entity.Property(e => e.DepId).HasColumnName("dep_id");

                entity.Property(e => e.DepName)
                    .IsRequired()
                    .HasColumnName("dep_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EnrollmentType>(entity =>
            {
                entity.HasKey(e => e.EntId);

                entity.HasIndex(e => e.EntTag)
                    .HasName("IX_EnrollmentType")
                    .IsUnique();

                entity.Property(e => e.EntId).HasColumnName("ent_id");

                entity.Property(e => e.EntName)
                    .IsRequired()
                    .HasColumnName("ent_name")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EntTag)
                    .IsRequired()
                    .HasColumnName("ent_tag")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Filter>(entity =>
            {
                entity.HasKey(e => e.FilId)
                    .HasName("PK_Filter");

                entity.ToTable("filter");

                entity.Property(e => e.FilId).HasColumnName("fil_id");

                entity.Property(e => e.FilCreateDate)
                    .HasColumnName("fil_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FilCreateUserId).HasColumnName("fil_create_user_id");

                entity.Property(e => e.FilDefaultValue)
                    .IsRequired()
                    .HasColumnName("fil_default_value")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.FilDeleteDate)
                    .HasColumnName("fil_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FilDeleteUserId).HasColumnName("fil_delete_user_id");

                entity.Property(e => e.FilMainOption).HasColumnName("fil_main_option");

                entity.Property(e => e.FilName)
                    .IsRequired()
                    .HasColumnName("fil_name")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FilRefreshData).HasColumnName("fil_refresh_data");

                entity.Property(e => e.FilTag)
                    .IsRequired()
                    .HasColumnName("fil_tag")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FilUpdateDate)
                    .HasColumnName("fil_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FilUpdateUserId).HasColumnName("fil_update_user_id");
            });

            modelBuilder.Entity<ForcedCombos>(entity =>
            {
                entity.HasKey(e => e.FcoId);

                entity.ToTable("Forced_Combos");

                entity.HasIndex(e => e.FcoPraNameA)
                    .HasName("IX_Forced_Combo_A");

                entity.HasIndex(e => e.FcoPraNameB)
                    .HasName("IX_Forced_Combo_B");

                entity.Property(e => e.FcoId).HasColumnName("fco_id");

                entity.Property(e => e.FcoPraNameA)
                    .IsRequired()
                    .HasColumnName("fco_pra_name_a")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.FcoPraNameB)
                    .IsRequired()
                    .HasColumnName("fco_pra_name_b")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Gender>(entity =>
            {
                entity.HasKey(e => e.GenId);

                entity.Property(e => e.GenId).HasColumnName("gen_id");

                entity.Property(e => e.GenName)
                    .HasColumnName("gen_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GenOldSex)
                    .HasColumnName("gen_old_sex")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HealthcareCenter>(entity =>
            {
                entity.HasKey(e => e.HecId);

                entity.ToTable("healthcare_center");

                entity.HasIndex(e => e.HecTag)
                    .HasName("IX_healthcare_center")
                    .IsUnique();

                entity.Property(e => e.HecId).HasColumnName("hec_id");

                entity.Property(e => e.HecAddressCityId).HasColumnName("hec_address_city_id");

                entity.Property(e => e.HecAddressCountryId).HasColumnName("hec_address_country_id");

                entity.Property(e => e.HecAddressLocationCode)
                    .HasColumnName("hec_address_location_code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HecAddressNumber)
                    .HasColumnName("hec_address_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HecAddressProvinceId).HasColumnName("hec_address_province_id");

                entity.Property(e => e.HecAddressStreet)
                    .HasColumnName("hec_address_street")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HecContactMail)
                    .IsRequired()
                    .HasColumnName("hec_contact_mail")
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.HecContactNumber)
                    .IsRequired()
                    .HasColumnName("hec_contact_number")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.HecCreateDate)
                    .HasColumnName("hec_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HecCreateUserId).HasColumnName("hec_create_user_id");

                entity.Property(e => e.HecDeleteDate)
                    .HasColumnName("hec_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HecDeleteUserId).HasColumnName("hec_delete_user_id");

                entity.Property(e => e.HecName)
                    .IsRequired()
                    .HasColumnName("hec_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HecOrganizationId).HasColumnName("hec_organization_id");

                entity.Property(e => e.HecTag)
                    .IsRequired()
                    .HasColumnName("hec_tag")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.HecUpdateDate)
                    .HasColumnName("hec_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HecUpdateUserId).HasColumnName("hec_update_user_id");

                entity.HasOne(d => d.HecAddressCity)
                    .WithMany(p => p.HealthcareCenter)
                    .HasForeignKey(d => d.HecAddressCityId)
                    .HasConstraintName("FK_healthcare_center_City");

                entity.HasOne(d => d.HecAddressCountry)
                    .WithMany(p => p.HealthcareCenter)
                    .HasForeignKey(d => d.HecAddressCountryId)
                    .HasConstraintName("FK_healthcare_center_Country");

                entity.HasOne(d => d.HecAddressProvince)
                    .WithMany(p => p.HealthcareCenter)
                    .HasForeignKey(d => d.HecAddressProvinceId)
                    .HasConstraintName("FK_healthcare_center_State");

                entity.HasOne(d => d.HecOrganization)
                    .WithMany(p => p.HealthcareCenter)
                    .HasForeignKey(d => d.HecOrganizationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_healthcare_center_organization");
            });

            modelBuilder.Entity<IdentificationType>(entity =>
            {
                entity.HasKey(e => e.IdtId);

                entity.ToTable("Identification_Type");

                entity.HasIndex(e => e.IdtName)
                    .HasName("IX_Identification_Type")
                    .IsUnique();

                entity.Property(e => e.IdtId).HasColumnName("idt_id");

                entity.Property(e => e.IdtName)
                    .HasColumnName("idt_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Info>(entity =>
            {
                entity.HasKey(e => e.InfId);

                entity.HasIndex(e => e.InfDiscriminator)
                    .HasName("IX_Info")
                    .IsUnique();

                entity.Property(e => e.InfId).HasColumnName("inf_id");

                entity.Property(e => e.InfDiscriminator)
                    .IsRequired()
                    .HasColumnName("inf_discriminator")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InfoSettings>(entity =>
            {
                entity.HasKey(e => e.InsId)
                    .HasName("PK_info_settings2");

                entity.ToTable("info_settings");

                entity.HasIndex(e => new { e.InsInfoId, e.InsKey })
                    .HasName("IX_info_settings")
                    .IsUnique();

                entity.Property(e => e.InsId).HasColumnName("ins_id");

                entity.Property(e => e.InsDescription)
                    .HasColumnName("ins_description")
                    .HasColumnType("text");

                entity.Property(e => e.InsInfoId).HasColumnName("ins_info_id");

                entity.Property(e => e.InsKey)
                    .IsRequired()
                    .HasColumnName("ins_key")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InsUpdateDate)
                    .HasColumnName("ins_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InsUpdateUserId).HasColumnName("ins_update_user_id");

                entity.Property(e => e.InsValue)
                    .HasColumnName("ins_value")
                    .HasColumnType("text");

                entity.HasOne(d => d.InsInfo)
                    .WithMany(p => p.InfoSettings)
                    .HasForeignKey(d => d.InsInfoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_info_settings_Info");

                entity.HasOne(d => d.InsUpdateUser)
                    .WithMany(p => p.InfoSettings)
                    .HasForeignKey(d => d.InsUpdateUserId)
                    .HasConstraintName("FK_info_settings_security_users");
            });

            modelBuilder.Entity<Insurance>(entity =>
            {
                entity.HasKey(e => e.InsId);

                entity.HasIndex(e => e.InsCode)
                    .HasName("UQ__Insurance__1367E606")
                    .IsUnique();

                entity.Property(e => e.InsId).HasColumnName("ins_id");

                entity.Property(e => e.InsCode)
                    .IsRequired()
                    .HasColumnName("ins_code")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsCreateDate)
                    .HasColumnName("ins_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InsCreateUserId).HasColumnName("ins_create_user_id");

                entity.Property(e => e.InsDeleteDate)
                    .HasColumnName("ins_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InsDeleteUserId).HasColumnName("ins_delete_user_id");

                entity.Property(e => e.InsName)
                    .IsRequired()
                    .HasColumnName("ins_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsPrivate).HasColumnName("ins_private");

                entity.Property(e => e.InsUpdateDate)
                    .HasColumnName("ins_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InsUpdateUserId).HasColumnName("ins_update_user_id");
            });

            modelBuilder.Entity<InsuranceAlias>(entity =>
            {
                entity.HasKey(e => e.IalId);

                entity.ToTable("Insurance_Alias");

                entity.HasIndex(e => e.IalName)
                    .HasName("UQ__Insuranc__8785FFA12FCF1A8A")
                    .IsUnique();

                entity.Property(e => e.IalId).HasColumnName("ial_id");

                entity.Property(e => e.IalInsuranceId).HasColumnName("ial_insurance_id");

                entity.Property(e => e.IalMain).HasColumnName("ial_main");

                entity.Property(e => e.IalName)
                    .IsRequired()
                    .HasColumnName("ial_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.IalInsurance)
                    .WithMany(p => p.InsuranceAlias)
                    .HasForeignKey(d => d.IalInsuranceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_alias_insurance");
            });

            modelBuilder.Entity<InsurancePlan>(entity =>
            {
                entity.HasKey(e => e.InpId);

                entity.ToTable("Insurance_plan");

                entity.HasIndex(e => e.InpCode)
                    .HasName("UQ__Insurance_plan__2B3F6F97")
                    .IsUnique();

                entity.Property(e => e.InpId).HasColumnName("inp_id");

                entity.Property(e => e.InpCode)
                    .IsRequired()
                    .HasColumnName("inp_code")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InpCreateDate)
                    .HasColumnName("inp_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InpCreateUserId).HasColumnName("inp_create_user_id");

                entity.Property(e => e.InpDeleteDate)
                    .HasColumnName("inp_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InpDeleteUserId).HasColumnName("inp_delete_user_id");

                entity.Property(e => e.InpInsuranceId).HasColumnName("inp_insurance_id");

                entity.Property(e => e.InpName)
                    .IsRequired()
                    .HasColumnName("inp_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InpUpdateDate)
                    .HasColumnName("inp_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InpUpdateUserId).HasColumnName("inp_update_user_id");

                entity.HasOne(d => d.InpInsurance)
                    .WithMany(p => p.InsurancePlan)
                    .HasForeignKey(d => d.InpInsuranceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ins_plan_insurance");
            });

            modelBuilder.Entity<InsurancePlanPatient>(entity =>
            {
                entity.HasKey(e => e.IppId);

                entity.ToTable("Insurance_plan_patient");

                entity.HasIndex(e => new { e.IppPatientId, e.IppInsaurancePlanId, e.IppDeleteDate })
                    .HasName("IX_Insurance_plan_patient")
                    .IsUnique();

                entity.Property(e => e.IppId).HasColumnName("ipp_id");

                entity.Property(e => e.IppCreateDate)
                    .HasColumnName("ipp_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IppCreateUserId).HasColumnName("ipp_create_user_id");

                entity.Property(e => e.IppDefault).HasColumnName("ipp_default");

                entity.Property(e => e.IppDeleteDate)
                    .HasColumnName("ipp_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IppDeleteUserId).HasColumnName("ipp_delete_user_id");

                entity.Property(e => e.IppInsaurancePlanId).HasColumnName("ipp_insaurance_plan_id");

                entity.Property(e => e.IppPatientId).HasColumnName("ipp_patient_id");

                entity.Property(e => e.IppUpdateDate)
                    .HasColumnName("ipp_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IppUpdateUserId).HasColumnName("ipp_update_user_id");

                entity.HasOne(d => d.IppInsaurancePlan)
                    .WithMany(p => p.InsurancePlanPatient)
                    .HasForeignKey(d => d.IppInsaurancePlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Insurance_patient_plan");

                entity.HasOne(d => d.IppPatient)
                    .WithMany(p => p.InsurancePlanPatient)
                    .HasForeignKey(d => d.IppPatientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Insurance_plan_patient");
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.HasKey(e => e.MesId);

                entity.HasIndex(e => e.MesTag)
                    .HasName("IX_Message_Practice_Group")
                    .IsUnique();

                entity.Property(e => e.MesId).HasColumnName("mes_id");

                entity.Property(e => e.MesTag)
                    .IsRequired()
                    .HasColumnName("mes_tag")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MesValue)
                    .IsRequired()
                    .HasColumnName("mes_value")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<MessageMobile>(entity =>
            {
                entity.HasKey(e => e.MebId);

                entity.ToTable("Message_Mobile");

                entity.Property(e => e.MebId).HasColumnName("meb_id");

                entity.Property(e => e.MebMessage)
                    .IsRequired()
                    .HasColumnName("meb_message");

                entity.Property(e => e.MebScreenName)
                    .IsRequired()
                    .HasColumnName("meb_screen_name")
                    .HasMaxLength(100);

                entity.Property(e => e.MebShowOnLoad).HasColumnName("meb_show_on_load");

                entity.Property(e => e.MebTag)
                    .IsRequired()
                    .HasColumnName("meb_tag")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MessagePracticeGroup>(entity =>
            {
                entity.HasKey(e => e.MpgId);

                entity.ToTable("Message_Practice_Group");

                entity.HasIndex(e => new { e.MpgMessageId, e.MpgPracticeGroupId })
                    .HasName("AK_Message_Practice_Group")
                    .IsUnique();

                entity.Property(e => e.MpgId).HasColumnName("mpg_id");

                entity.Property(e => e.MpgMessageId).HasColumnName("mpg_message_id");

                entity.Property(e => e.MpgOrder).HasColumnName("mpg_order");

                entity.Property(e => e.MpgPracticeGroupId).HasColumnName("mpg_practice_group_id");

                entity.HasOne(d => d.MpgMessage)
                    .WithMany(p => p.MessagePracticeGroup)
                    .HasForeignKey(d => d.MpgMessageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MPG_Message");

                entity.HasOne(d => d.MpgPracticeGroup)
                    .WithMany(p => p.MessagePracticeGroup)
                    .HasForeignKey(d => d.MpgPracticeGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MPG_Practice_Group");
            });

            modelBuilder.Entity<MobileLog>(entity =>
            {
                entity.HasKey(e => e.MolId);

                entity.Property(e => e.MolId).HasColumnName("mol_id");

                entity.Property(e => e.MolDatetime)
                    .HasColumnName("mol_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.MolDetail)
                    .IsRequired()
                    .HasColumnName("mol_detail");

                entity.Property(e => e.MolMobileSessionId).HasColumnName("mol_mobile_session_id");

                entity.Property(e => e.MolPayload).HasColumnName("mol_payload");
            });

            modelBuilder.Entity<MobileService>(entity =>
            {
                entity.HasKey(e => e.MsrId);

                entity.Property(e => e.MsrId).HasColumnName("msr_id");

                entity.Property(e => e.MsrCreateDate)
                    .HasColumnName("msr_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.MsrCreateUserId).HasColumnName("msr_create_user_id");

                entity.Property(e => e.MsrDeleteDate)
                    .HasColumnName("msr_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.MsrDeleteUserId).HasColumnName("msr_delete_user_id");

                entity.Property(e => e.MsrDescriptionHtml).HasColumnName("msr_description_html");

                entity.Property(e => e.MsrIconRelativePath)
                    .HasColumnName("msr_icon_relative_path")
                    .HasMaxLength(255);

                entity.Property(e => e.MsrImageRelativePath)
                    .HasColumnName("msr_image_relative_path")
                    .HasMaxLength(255);

                entity.Property(e => e.MsrTitle)
                    .IsRequired()
                    .HasColumnName("msr_title")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<MobileSession>(entity =>
            {
                entity.HasKey(e => e.MosId);

                entity.Property(e => e.MosId).HasColumnName("mos_id");

                entity.Property(e => e.MosCreateDate)
                    .HasColumnName("mos_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.MosCreateUserId).HasColumnName("mos_create_user_id");

                entity.Property(e => e.MosDeleteDate)
                    .HasColumnName("mos_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.MosDeleteUserId).HasColumnName("mos_delete_user_id");

                entity.Property(e => e.MosDeviceOs)
                    .HasColumnName("mos_device_os")
                    .HasMaxLength(50);

                entity.Property(e => e.MosExpirationDate)
                    .HasColumnName("mos_expiration_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.MosIpAddress)
                    .IsRequired()
                    .HasColumnName("mos_ip_address")
                    .HasMaxLength(1000);

                entity.Property(e => e.MosPushToken)
                    .HasColumnName("mos_push_token")
                    .HasMaxLength(1000);

                entity.Property(e => e.MosToken)
                    .IsRequired()
                    .HasColumnName("mos_token")
                    .HasMaxLength(1000);

                entity.Property(e => e.MosUserDni)
                    .HasColumnName("mos_user_dni")
                    .HasMaxLength(50);

                entity.Property(e => e.MosUserId).HasColumnName("mos_user_id");
            });

            modelBuilder.Entity<OffersDistribution>(entity =>
            {
                entity.HasKey(e => e.OfdId);

                entity.ToTable("offers_distribution");

                entity.HasIndex(e => e.OfdOrder)
                    .HasName("IX_offers_distribution")
                    .IsUnique();

                entity.Property(e => e.OfdId).HasColumnName("ofd_id");

                entity.Property(e => e.OfdOffersAmount).HasColumnName("ofd_offers_amount");

                entity.Property(e => e.OfdOrder).HasColumnName("ofd_order");

                entity.Property(e => e.OfdPercentage)
                    .HasColumnName("ofd_percentage")
                    .HasColumnType("decimal(3, 1)");
            });

            modelBuilder.Entity<Operation>(entity =>
            {
                entity.HasKey(e => e.OpeId);

                entity.ToTable("operation");

                entity.HasIndex(e => e.OpeCode)
                    .HasName("UQ__operatio__9589437A06CD04F7")
                    .IsUnique();

                entity.Property(e => e.OpeId).HasColumnName("ope_id");

                entity.Property(e => e.OpeCaptchaAttempts).HasColumnName("ope_captcha_attempts");

                entity.Property(e => e.OpeCaptchaAttemptsSeconds).HasColumnName("ope_captcha_attempts_seconds");

                entity.Property(e => e.OpeCode)
                    .IsRequired()
                    .HasColumnName("ope_code")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.OpeLockAttempts).HasColumnName("ope_lock_attempts");

                entity.Property(e => e.OpeLockAttemptsSeconds).HasColumnName("ope_lock_attempts_seconds");

                entity.Property(e => e.OpeName)
                    .IsRequired()
                    .HasColumnName("ope_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OperationLog>(entity =>
            {
                entity.HasKey(e => e.OplId);

                entity.ToTable("operation_log");

                entity.Property(e => e.OplId).HasColumnName("opl_id");

                entity.Property(e => e.OplAditionalInformation)
                    .HasColumnName("opl_aditional_information")
                    .HasMaxLength(6000)
                    .IsUnicode(false);

                entity.Property(e => e.OplCreateDate)
                    .HasColumnName("opl_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.OplCreateUserId).HasColumnName("opl_create_user_id");

                entity.Property(e => e.OplDate)
                    .HasColumnName("opl_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.OplDeleteDate)
                    .HasColumnName("opl_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.OplDeleteUserId).HasColumnName("opl_delete_user_id");

                entity.Property(e => e.OplOperationId).HasColumnName("opl_operation_id");

                entity.Property(e => e.OplOrigin)
                    .HasColumnName("opl_origin")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OplPatientId).HasColumnName("opl_patient_id");

                entity.Property(e => e.OplUpdateDate)
                    .HasColumnName("opl_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.OplUpdateUserId).HasColumnName("opl_update_user_id");

                entity.Property(e => e.OplUserId).HasColumnName("opl_user_id");
            });

            modelBuilder.Entity<Operator>(entity =>
            {
                entity.HasKey(e => e.OpeId);

                entity.HasIndex(e => e.OpePatientId)
                    .HasName("IX_Operator")
                    .IsUnique();

                entity.Property(e => e.OpeId).HasColumnName("ope_id");

                entity.Property(e => e.OpeLaboralIdentification)
                    .HasColumnName("ope_laboral_identification")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OpeMail)
                    .IsRequired()
                    .HasColumnName("ope_mail")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OpeOrganizationId).HasColumnName("ope_organization_id");

                entity.Property(e => e.OpePatientId).HasColumnName("ope_patient_id");

                entity.HasOne(d => d.OpePatient)
                    .WithOne(p => p.Operator)
                    .HasForeignKey<Operator>(d => d.OpePatientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Operator_patient");
            });

            modelBuilder.Entity<Organization>(entity =>
            {
                entity.HasKey(e => e.OrgId);

                entity.ToTable("organization");

                entity.HasIndex(e => new { e.OrgName, e.OrgDeleteDate })
                    .HasName("IX_organization")
                    .IsUnique();

                entity.Property(e => e.OrgId).HasColumnName("org_id");

                entity.Property(e => e.OrgCreateDate)
                    .HasColumnName("org_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrgCreateUserId).HasColumnName("org_create_user_id");

                entity.Property(e => e.OrgDeleteDate)
                    .HasColumnName("org_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrgDeleteUserId).HasColumnName("org_delete_user_id");

                entity.Property(e => e.OrgLevel).HasColumnName("org_level");

                entity.Property(e => e.OrgName)
                    .IsRequired()
                    .HasColumnName("org_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrgParentId).HasColumnName("org_parent_id");

                entity.Property(e => e.OrgUpdateDate)
                    .HasColumnName("org_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrgUpdateUserId).HasColumnName("org_update_user_id");

                entity.HasOne(d => d.OrgParent)
                    .WithMany(p => p.InverseOrgParent)
                    .HasForeignKey(d => d.OrgParentId)
                    .HasConstraintName("FK_organization_organization");
            });

            modelBuilder.Entity<Patient>(entity =>
            {
                entity.HasKey(e => e.PatId);

                entity.ToTable("patient");

                entity.HasIndex(e => new { e.PatIdentificationTypeId, e.PatIdentificactionNumber, e.PatDeleteDate })
                    .HasName("unique_patient")
                    .IsUnique();

                entity.Property(e => e.PatId).HasColumnName("pat_id");

                entity.Property(e => e.PatAddressCityId).HasColumnName("pat_address_city_id");

                entity.Property(e => e.PatAddressCountryId).HasColumnName("pat_address_country_id");

                entity.Property(e => e.PatAddressLocationCode)
                    .HasColumnName("pat_address_location_code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PatAddressNumber)
                    .HasColumnName("pat_address_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PatAddressStateId).HasColumnName("pat_address_state_id");

                entity.Property(e => e.PatAddressStreet)
                    .HasColumnName("pat_address_street")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PatClaustrophobic).HasColumnName("pat_claustrophobic");

                entity.Property(e => e.PatCreateDate)
                    .HasColumnName("pat_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PatCreateUserId).HasColumnName("pat_create_user_id");

                entity.Property(e => e.PatDateOfBirth)
                    .HasColumnName("pat_date_of_birth")
                    .HasColumnType("datetime");

                entity.Property(e => e.PatDeleteDate)
                    .HasColumnName("pat_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PatDeleteUserId).HasColumnName("pat_delete_user_id");

                entity.Property(e => e.PatFirstName)
                    .IsRequired()
                    .HasColumnName("pat_first_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PatIdentificactionNumber).HasColumnName("pat_identificaction_number");

                entity.Property(e => e.PatIdentificationTypeId).HasColumnName("pat_identification_type_id");

                entity.Property(e => e.PatLastName)
                    .IsRequired()
                    .HasColumnName("pat_last_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PatMail)
                    .HasColumnName("pat_mail")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PatPhoneNumber1)
                    .HasColumnName("pat_phone_number1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PatPhoneNumber1Area)
                    .HasColumnName("pat_phone_number_1_area")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.PatPhoneNumber2)
                    .HasColumnName("pat_phone_number2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PatPhoneNumber3)
                    .HasColumnName("pat_phone_number_3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PatRequiresUpdate)
                    .IsRequired()
                    .HasColumnName("pat_requires_update")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.PatSex)
                    .HasColumnName("pat_sex")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PatUpdateDate)
                    .HasColumnName("pat_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PatUpdateUserId).HasColumnName("pat_update_user_id");

                entity.Property(e => e.PatZipcode)
                    .HasColumnName("pat_zipcode")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.PatAddressCity)
                    .WithMany(p => p.Patient)
                    .HasForeignKey(d => d.PatAddressCityId)
                    .HasConstraintName("FK_patient_City");

                entity.HasOne(d => d.PatAddressCountry)
                    .WithMany(p => p.Patient)
                    .HasForeignKey(d => d.PatAddressCountryId)
                    .HasConstraintName("FK_patient_Country");

                entity.HasOne(d => d.PatAddressState)
                    .WithMany(p => p.Patient)
                    .HasForeignKey(d => d.PatAddressStateId)
                    .HasConstraintName("FK_patient_State");

                entity.HasOne(d => d.PatIdentificationType)
                    .WithMany(p => p.Patient)
                    .HasForeignKey(d => d.PatIdentificationTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_patient_Identification_Type");
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.HasKey(e => e.PerId);

                entity.ToTable("person");

                entity.Property(e => e.PerId).HasColumnName("per_id");

                entity.Property(e => e.PerName)
                    .IsRequired()
                    .HasColumnName("per_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PerSurname)
                    .IsRequired()
                    .HasColumnName("per_surname")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Physician>(entity =>
            {
                entity.HasKey(e => e.PhyId);

                entity.HasIndex(e => e.PhyPatientId)
                    .HasName("IX_Physician")
                    .IsUnique();

                entity.Property(e => e.PhyId).HasColumnName("phy_id");

                entity.Property(e => e.PhyMail)
                    .IsRequired()
                    .HasColumnName("phy_mail")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PhyNationalEnrollment)
                    .HasColumnName("phy_national_enrollment")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhyPatientId).HasColumnName("phy_patient_id");

                entity.Property(e => e.PhyPhone)
                    .HasColumnName("phy_phone")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhyProvincialEnrollment)
                    .HasColumnName("phy_provincial_enrollment")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhySpecialismEnrollment)
                    .HasColumnName("phy_specialism_enrollment")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.PhyPatient)
                    .WithOne(p => p.Physician)
                    .HasForeignKey<Physician>(d => d.PhyPatientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Physician_patient");
            });

            modelBuilder.Entity<Practice>(entity =>
            {
                entity.HasKey(e => e.PraId);

                entity.HasIndex(e => e.PraCode)
                    .HasName("UQ__Practice__82283FE901D345B0")
                    .IsUnique();

                entity.Property(e => e.PraId).HasColumnName("pra_id");

                entity.Property(e => e.PraCode)
                    .HasColumnName("pra_code")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PraCreateDate)
                    .HasColumnName("pra_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PraCreateUserId).HasColumnName("pra_create_user_id");

                entity.Property(e => e.PraDeleteDate)
                    .HasColumnName("pra_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PraDeleteUserId).HasColumnName("pra_delete_user_id");

                entity.Property(e => e.PraIsHidden).HasColumnName("pra_is_hidden");

                entity.Property(e => e.PraName)
                    .IsRequired()
                    .HasColumnName("pra_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PraPracticeContrastId).HasColumnName("pra_practice_contrast_id");

                entity.Property(e => e.PraPracticeSubsequentId).HasColumnName("pra_practice_subsequent_id");

                entity.Property(e => e.PraRanking).HasColumnName("pra_ranking");

                entity.Property(e => e.PraServiceId).HasColumnName("pra_service_id");

                entity.Property(e => e.PraUpdateDate)
                    .HasColumnName("pra_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PraUpdateUserId).HasColumnName("pra_update_user_id");

                entity.HasOne(d => d.PraPracticeContrast)
                    .WithMany(p => p.InversePraPracticeContrast)
                    .HasForeignKey(d => d.PraPracticeContrastId)
                    .HasConstraintName("FK_Practice_Practice");

                entity.HasOne(d => d.PraPracticeSubsequent)
                    .WithMany(p => p.InversePraPracticeSubsequent)
                    .HasForeignKey(d => d.PraPracticeSubsequentId)
                    .HasConstraintName("FK_Practice_Practice1");

                entity.HasOne(d => d.PraService)
                    .WithMany(p => p.Practice)
                    .HasForeignKey(d => d.PraServiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Practice_service");
            });

            modelBuilder.Entity<PracticeAlias>(entity =>
            {
                entity.HasKey(e => e.PalId);

                entity.ToTable("Practice_Alias");

                entity.HasIndex(e => e.PalName)
                    .HasName("UQ__Practice_Alias__4BAC3F29")
                    .IsUnique();

                entity.Property(e => e.PalId).HasColumnName("pal_id");

                entity.Property(e => e.PalMainId).HasColumnName("pal_main_id");

                entity.Property(e => e.PalName)
                    .IsRequired()
                    .HasColumnName("pal_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PalPracticeId).HasColumnName("pal_practice_id");

                entity.HasOne(d => d.PalPractice)
                    .WithMany(p => p.PracticeAlias)
                    .HasForeignKey(d => d.PalPracticeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_alias_practice");
            });

            modelBuilder.Entity<PracticeFromPreTranslationRule>(entity =>
            {
                entity.HasKey(e => e.PfrId);

                entity.ToTable("PracticeFrom_PreTranslationRule");

                entity.HasIndex(e => new { e.PfrRuleId, e.PfrPracticeId })
                    .HasName("IX_PracticeFrom_PreTranslationRule")
                    .IsUnique();

                entity.Property(e => e.PfrId).HasColumnName("pfr_id");

                entity.Property(e => e.PfrPracticeId).HasColumnName("pfr_practice_id");

                entity.Property(e => e.PfrRuleId).HasColumnName("pfr_rule_id");

                entity.HasOne(d => d.PfrPractice)
                    .WithMany(p => p.PracticeFromPreTranslationRule)
                    .HasForeignKey(d => d.PfrPracticeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PracticeFrom_PreTranslationRule_Practice");

                entity.HasOne(d => d.PfrRule)
                    .WithMany(p => p.PracticeFromPreTranslationRule)
                    .HasForeignKey(d => d.PfrRuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PracticeFrom_PreTranslationRule_Pretranslation_Rule");
            });

            modelBuilder.Entity<PracticeGroup>(entity =>
            {
                entity.HasKey(e => e.PrgId);

                entity.ToTable("Practice_group");

                entity.HasIndex(e => e.PrgName)
                    .HasName("IX_Practice_group")
                    .IsUnique();

                entity.Property(e => e.PrgId).HasColumnName("prg_id");

                entity.Property(e => e.PrgName)
                    .IsRequired()
                    .HasColumnName("prg_name")
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PracticeGroupPractice>(entity =>
            {
                entity.HasKey(e => e.PgpId);

                entity.ToTable("Practice_group_practice");

                entity.HasIndex(e => new { e.PgpPracticeGroupId, e.PgpPracticeId })
                    .HasName("IX_Practice_group_practice")
                    .IsUnique();

                entity.Property(e => e.PgpId).HasColumnName("pgp_id");

                entity.Property(e => e.PgpPracticeGroupId).HasColumnName("pgp_practice_group_id");

                entity.Property(e => e.PgpPracticeId).HasColumnName("pgp_practice_id");

                entity.HasOne(d => d.PgpPracticeGroup)
                    .WithMany(p => p.PracticeGroupPractice)
                    .HasForeignKey(d => d.PgpPracticeGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Practice_group_practice_Practice_group");

                entity.HasOne(d => d.PgpPractice)
                    .WithMany(p => p.PracticeGroupPractice)
                    .HasForeignKey(d => d.PgpPracticeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Practice_group_practice_Practice");
            });

            modelBuilder.Entity<PracticeOrganization>(entity =>
            {
                entity.HasKey(e => e.ProId)
                    .HasName("PK_Practice_organization");

                entity.ToTable("practice_organization");

                entity.HasIndex(e => new { e.ProPracticeId, e.ProOrganizationId, e.ProDeleteDate })
                    .HasName("IX_practice_organization")
                    .IsUnique();

                entity.Property(e => e.ProId).HasColumnName("pro_id");

                entity.Property(e => e.ProCreateDate)
                    .HasColumnName("pro_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProCreateUserId).HasColumnName("pro_create_user_id");

                entity.Property(e => e.ProDeleteDate)
                    .HasColumnName("pro_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProDeleteUserId).HasColumnName("pro_delete_user_id");

                entity.Property(e => e.ProOrganizationId).HasColumnName("pro_organization_id");

                entity.Property(e => e.ProPracticeId).HasColumnName("pro_practice_id");

                entity.Property(e => e.ProReservationTypeId).HasColumnName("pro_reservation_type_id");

                entity.Property(e => e.ProUpdateDate)
                    .HasColumnName("pro_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProUpdateUserId).HasColumnName("pro_update_user_id");

                entity.HasOne(d => d.ProOrganization)
                    .WithMany(p => p.PracticeOrganization)
                    .HasForeignKey(d => d.ProOrganizationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_practorg_organization");

                entity.HasOne(d => d.ProPractice)
                    .WithMany(p => p.PracticeOrganization)
                    .HasForeignKey(d => d.ProPracticeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_practorg_practice");

                entity.HasOne(d => d.ProReservationType)
                    .WithMany(p => p.PracticeOrganization)
                    .HasForeignKey(d => d.ProReservationTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_practorg_reservation");
            });

            modelBuilder.Entity<PracticeResponseQuestionPracticeGroup>(entity =>
            {
                entity.HasKey(e => e.PrqId);

                entity.ToTable("Practice_response_question_practice_group");

                entity.HasIndex(e => new { e.PrqResponseQuestionPracticeGroupId, e.PrqPracticeAddedId })
                    .HasName("IX_Practice_response_question_practice_group")
                    .IsUnique();

                entity.Property(e => e.PrqId).HasColumnName("prq_id");

                entity.Property(e => e.PrqPracticeAddedId).HasColumnName("prq_practice_added_id");

                entity.Property(e => e.PrqResponseQuestionPracticeGroupId).HasColumnName("prq_response_question_practice_group_id");

                entity.HasOne(d => d.PrqPracticeAdded)
                    .WithMany(p => p.PracticeResponseQuestionPracticeGroup)
                    .HasForeignKey(d => d.PrqPracticeAddedId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Practice_response_question_practice_group_Practice");

                entity.HasOne(d => d.PrqResponseQuestionPracticeGroup)
                    .WithMany(p => p.PracticeResponseQuestionPracticeGroup)
                    .HasForeignKey(d => d.PrqResponseQuestionPracticeGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Practice_response_question_practice_group_Response_question_practice_group");
            });

            modelBuilder.Entity<PracticeRestriction>(entity =>
            {
                entity.HasKey(e => e.PreId);

                entity.ToTable("Practice_restriction");

                entity.HasIndex(e => new { e.PrePracticeRestrictionTypeId, e.PreItemId, e.PrePracticeRestrictedId })
                    .HasName("IX_Practice_restriction");

                entity.Property(e => e.PreId).HasColumnName("pre_id");

                entity.Property(e => e.PreItemId)
                    .HasColumnName("pre_item_id")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrePracticeRestrictedId).HasColumnName("pre_practice_restricted_id");

                entity.Property(e => e.PrePracticeRestrictionTypeId).HasColumnName("pre_practice_restriction_type_id");

                entity.HasOne(d => d.PrePracticeRestricted)
                    .WithMany(p => p.PracticeRestriction)
                    .HasForeignKey(d => d.PrePracticeRestrictedId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Practice_restriction_Practice");

                entity.HasOne(d => d.PrePracticeRestrictionType)
                    .WithMany(p => p.PracticeRestriction)
                    .HasForeignKey(d => d.PrePracticeRestrictionTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Practice_restriction_Practice_restriction_type");
            });

            modelBuilder.Entity<PracticeRestrictionType>(entity =>
            {
                entity.HasKey(e => e.PrtId);

                entity.ToTable("Practice_restriction_type");

                entity.HasIndex(e => e.PrtName)
                    .HasName("IX_Practice_restriction_type")
                    .IsUnique();

                entity.Property(e => e.PrtId).HasColumnName("prt_id");

                entity.Property(e => e.PrtMessageTag)
                    .HasColumnName("prt_message_tag")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrtName)
                    .IsRequired()
                    .HasColumnName("prt_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PracticeToPreTranslationRule>(entity =>
            {
                entity.HasKey(e => e.PtrId);

                entity.ToTable("PracticeTo_PreTranslationRule");

                entity.HasIndex(e => new { e.PtrRuleId, e.PtrPracticeId })
                    .HasName("IX_PracticeTo_PreTranslationRule")
                    .IsUnique();

                entity.Property(e => e.PtrId).HasColumnName("ptr_id");

                entity.Property(e => e.PtrPracticeId).HasColumnName("ptr_practice_id");

                entity.Property(e => e.PtrRuleId).HasColumnName("ptr_rule_id");

                entity.HasOne(d => d.PtrPractice)
                    .WithMany(p => p.PracticeToPreTranslationRule)
                    .HasForeignKey(d => d.PtrPracticeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PracticeTo_PreTranslationRule_Practice");

                entity.HasOne(d => d.PtrRule)
                    .WithMany(p => p.PracticeToPreTranslationRule)
                    .HasForeignKey(d => d.PtrRuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PracticeTo_PreTranslationRule_PreTranslation_Rule");
            });

            modelBuilder.Entity<PreTranslationLog>(entity =>
            {
                entity.HasKey(e => e.PtlId);

                entity.ToTable("PreTranslation_Log");

                entity.Property(e => e.PtlId).HasColumnName("ptl_id");

                entity.Property(e => e.PtlCreationDate)
                    .HasColumnName("ptl_creation_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PtlCreationUserId).HasColumnName("ptl_creation_user_id");

                entity.Property(e => e.PtlInsurancePlanId).HasColumnName("ptl_insurance_plan_id");

                entity.Property(e => e.PtlObservations)
                    .HasColumnName("ptl_observations")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.PtlOrganizationId).HasColumnName("ptl_organization_id");

                entity.Property(e => e.PtlPatientId).HasColumnName("ptl_patient_id");

                entity.Property(e => e.PtlPracticesFromIds)
                    .IsRequired()
                    .HasColumnName("ptl_practices_from_ids")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.PtlPracticesToIds)
                    .HasColumnName("ptl_practices_to_ids")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.PtlUpdateDate)
                    .HasColumnName("ptl_update_date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.PtlCreationUser)
                    .WithMany(p => p.PreTranslationLog)
                    .HasForeignKey(d => d.PtlCreationUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PreTranslation_Log_security_users");

                entity.HasOne(d => d.PtlInsurancePlan)
                    .WithMany(p => p.PreTranslationLog)
                    .HasForeignKey(d => d.PtlInsurancePlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PreTranslation_Log_Insurance_plan");

                entity.HasOne(d => d.PtlOrganization)
                    .WithMany(p => p.PreTranslationLog)
                    .HasForeignKey(d => d.PtlOrganizationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PreTranslation_Log_organization");

                entity.HasOne(d => d.PtlPatient)
                    .WithMany(p => p.PreTranslationLog)
                    .HasForeignKey(d => d.PtlPatientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PreTranslation_Log_patient");
            });

            modelBuilder.Entity<PreTranslationLogDetail>(entity =>
            {
                entity.HasKey(e => e.PldId);

                entity.ToTable("PreTranslation_Log_Detail");

                entity.Property(e => e.PldId).HasColumnName("pld_id");

                entity.Property(e => e.PldLogId).HasColumnName("pld_log_id");

                entity.Property(e => e.PldRuleAppliedId).HasColumnName("pld_rule_applied_id");

                entity.HasOne(d => d.PldLog)
                    .WithMany(p => p.PreTranslationLogDetail)
                    .HasForeignKey(d => d.PldLogId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PreTranslation_Log_Detail_PreTranslation_Log");
            });

            modelBuilder.Entity<PreTranslationRule>(entity =>
            {
                entity.HasKey(e => e.PtrId);

                entity.ToTable("PreTranslation_Rule");

                entity.HasIndex(e => new { e.PtrName, e.PtrDeleteDate })
                    .HasName("IX_PreTranslation_Rule")
                    .IsUnique();

                entity.Property(e => e.PtrId).HasColumnName("ptr_id");

                entity.Property(e => e.PtrAgeCondition)
                    .HasColumnName("ptr_age_condition")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PtrApplyPartialMatch)
                    .HasColumnName("ptr_apply_partial_match")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PtrCreateDate)
                    .HasColumnName("ptr_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PtrDeleteDate)
                    .HasColumnName("ptr_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PtrExcludesInsurance).HasColumnName("ptr_excludes_insurance");

                entity.Property(e => e.PtrInsuranceId).HasColumnName("ptr_insurance_id");

                entity.Property(e => e.PtrName)
                    .IsRequired()
                    .HasColumnName("ptr_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PtrObservations)
                    .HasColumnName("ptr_observations")
                    .HasMaxLength(2000);

                entity.Property(e => e.PtrOnlyExactGroup).HasColumnName("ptr_only_exact_group");

                entity.Property(e => e.PtrOrganizationId).HasColumnName("ptr_organization_id");

                entity.Property(e => e.PtrRuleTypeId).HasColumnName("ptr_rule_type_id");

                entity.Property(e => e.PtrServiceId).HasColumnName("ptr_service_id");

                entity.HasOne(d => d.PtrInsurance)
                    .WithMany(p => p.PreTranslationRule)
                    .HasForeignKey(d => d.PtrInsuranceId)
                    .HasConstraintName("FK_PreTranslation_Rule_Insurance");

                entity.HasOne(d => d.PtrOrganization)
                    .WithMany(p => p.PreTranslationRule)
                    .HasForeignKey(d => d.PtrOrganizationId)
                    .HasConstraintName("FK_PreTranslation_Rule_organization");

                entity.HasOne(d => d.PtrRuleType)
                    .WithMany(p => p.PreTranslationRule)
                    .HasForeignKey(d => d.PtrRuleTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PreTranslation_Rule_Rule_Type");

                entity.HasOne(d => d.PtrService)
                    .WithMany(p => p.PreTranslationRule)
                    .HasForeignKey(d => d.PtrServiceId)
                    .HasConstraintName("FK_PreTranslation_Rule_Service");
            });

            modelBuilder.Entity<Question>(entity =>
            {
                entity.HasKey(e => e.QueId);

                entity.HasIndex(e => e.QueName)
                    .HasName("IX_Question")
                    .IsUnique();

                entity.Property(e => e.QueId).HasColumnName("que_id");

                entity.Property(e => e.QueName)
                    .IsRequired()
                    .HasColumnName("que_name")
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<QuestionPracticeGroup>(entity =>
            {
                entity.HasKey(e => e.QpgId);

                entity.ToTable("Question_practice_group");

                entity.HasIndex(e => new { e.QpgQuestionId, e.QpgPracticeGroupId })
                    .HasName("IX_Question_practice_group")
                    .IsUnique();

                entity.Property(e => e.QpgId).HasColumnName("qpg_id");

                entity.Property(e => e.QpgOrder).HasColumnName("qpg_order");

                entity.Property(e => e.QpgPracticeGroupId).HasColumnName("qpg_practice_group_id");

                entity.Property(e => e.QpgQuestionId).HasColumnName("qpg_question_id");

                entity.HasOne(d => d.QpgPracticeGroup)
                    .WithMany(p => p.QuestionPracticeGroup)
                    .HasForeignKey(d => d.QpgPracticeGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Question_practice_group_Practice_group");

                entity.HasOne(d => d.QpgQuestion)
                    .WithMany(p => p.QuestionPracticeGroup)
                    .HasForeignKey(d => d.QpgQuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Question_practice_group_Question");
            });

            modelBuilder.Entity<RelationshipType>(entity =>
            {
                entity.HasKey(e => e.RetId);

                entity.ToTable("Relationship_type");

                entity.HasIndex(e => e.RetName)
                    .HasName("IX_Relationship_Type_1")
                    .IsUnique();

                entity.Property(e => e.RetId).HasColumnName("ret_id");

                entity.Property(e => e.RetName)
                    .IsRequired()
                    .HasColumnName("ret_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportStatus>(entity =>
            {
                entity.HasKey(e => e.ResId);

                entity.ToTable("report_status");

                entity.HasIndex(e => e.ResTag)
                    .HasName("IX_report_status")
                    .IsUnique();

                entity.Property(e => e.ResId).HasColumnName("res_id");

                entity.Property(e => e.ResName)
                    .HasColumnName("res_name")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.ResTag)
                    .IsRequired()
                    .HasColumnName("res_tag")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Representative>(entity =>
            {
                entity.HasKey(e => e.RepId);

                entity.Property(e => e.RepId).HasColumnName("rep_id");

                entity.Property(e => e.RepCorporativeEmail)
                    .IsRequired()
                    .HasColumnName("rep_corporative_email")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RepPatientId).HasColumnName("rep_patient_id");

                entity.HasOne(d => d.RepPatient)
                    .WithMany(p => p.Representative)
                    .HasForeignKey(d => d.RepPatientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Representative_patient");
            });

            modelBuilder.Entity<RepresentativeHealthCareCenter>(entity =>
            {
                entity.HasKey(e => e.RhcId);

                entity.ToTable("Representative_HealthCareCenter");

                entity.Property(e => e.RhcId).HasColumnName("rhc_id");

                entity.Property(e => e.RhcHealthCareCenterId).HasColumnName("rhc_healthCare_Center_id");

                entity.Property(e => e.RhcRepresentativeId).HasColumnName("rhc_representative_id");

                entity.HasOne(d => d.RhcHealthCareCenter)
                    .WithMany(p => p.RepresentativeHealthCareCenter)
                    .HasForeignKey(d => d.RhcHealthCareCenterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Representative_HealthCareCenter_HealthCare_Center");

                entity.HasOne(d => d.RhcRepresentative)
                    .WithMany(p => p.RepresentativeHealthCareCenter)
                    .HasForeignKey(d => d.RhcRepresentativeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Representative_HealthCareCenter_Representative");
            });

            modelBuilder.Entity<RepresentativeInsurance>(entity =>
            {
                entity.HasKey(e => e.RinId);

                entity.ToTable("Representative_Insurance");

                entity.Property(e => e.RinId).HasColumnName("rin_id");

                entity.Property(e => e.RinInsuranceId).HasColumnName("rin_insurance_id");

                entity.Property(e => e.RinRepresentativeId).HasColumnName("rin_representative_id");

                entity.HasOne(d => d.RinInsurance)
                    .WithMany(p => p.RepresentativeInsurance)
                    .HasForeignKey(d => d.RinInsuranceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Representative_Insurance_Insurance");

                entity.HasOne(d => d.RinRepresentative)
                    .WithMany(p => p.RepresentativeInsurance)
                    .HasForeignKey(d => d.RinRepresentativeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Representative_Insurance_Representative");
            });

            modelBuilder.Entity<RepresentativeService>(entity =>
            {
                entity.HasKey(e => e.ResId);

                entity.ToTable("Representative_Service");

                entity.Property(e => e.ResId).HasColumnName("res_id");

                entity.Property(e => e.ResRepresentativeId).HasColumnName("res_representative_id");

                entity.Property(e => e.ResServiceId).HasColumnName("res_service_id");

                entity.HasOne(d => d.ResRepresentative)
                    .WithMany(p => p.RepresentativeService)
                    .HasForeignKey(d => d.ResRepresentativeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Representative_Service_Representative");

                entity.HasOne(d => d.ResService)
                    .WithMany(p => p.RepresentativeService)
                    .HasForeignKey(d => d.ResServiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Representative_Service_Service");
            });

            modelBuilder.Entity<ReservationType>(entity =>
            {
                entity.HasKey(e => e.RetId);

                entity.ToTable("Reservation_Type");

                entity.HasIndex(e => e.RetTag)
                    .HasName("IX_Reservation_Type")
                    .IsUnique();

                entity.Property(e => e.RetId).HasColumnName("ret_id");

                entity.Property(e => e.RetName)
                    .IsRequired()
                    .HasColumnName("ret_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RetPriority).HasColumnName("ret_priority");

                entity.Property(e => e.RetTag)
                    .IsRequired()
                    .HasColumnName("ret_tag")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ResponseQuestionPracticeGroup>(entity =>
            {
                entity.HasKey(e => e.RqpId);

                entity.ToTable("Response_question_practice_group");

                entity.HasIndex(e => new { e.RqpQuestionPracticeGroupId, e.RqpResponseText })
                    .HasName("IX_Response_question_practice_group")
                    .IsUnique();

                entity.Property(e => e.RqpId).HasColumnName("rqp_id");

                entity.Property(e => e.RqpForcedClaustrophobicCondition).HasColumnName("rqp_forced_claustrophobic_condition");

                entity.Property(e => e.RqpQuestionPracticeGroupId).HasColumnName("rqp_question_practice_group_id");

                entity.Property(e => e.RqpResponseText)
                    .IsRequired()
                    .HasColumnName("rqp_response_text")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.RqpQuestionPracticeGroup)
                    .WithMany(p => p.ResponseQuestionPracticeGroup)
                    .HasForeignKey(d => d.RqpQuestionPracticeGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Response_question_practice_group_Question_practice_group");
            });

            modelBuilder.Entity<RuleType>(entity =>
            {
                entity.HasKey(e => e.RtyId);

                entity.ToTable("Rule_Type");

                entity.HasIndex(e => e.RtyName)
                    .HasName("IX_Rule_Type")
                    .IsUnique();

                entity.Property(e => e.RtyId).HasColumnName("rty_id");

                entity.Property(e => e.RtyApplicationOrder).HasColumnName("rty_application_order");

                entity.Property(e => e.RtyName)
                    .IsRequired()
                    .HasColumnName("rty_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Script>(entity =>
            {
                entity.HasKey(e => e.ScrId);

                entity.ToTable("script");

                entity.HasIndex(e => e.ScrTag)
                    .HasName("IX_script")
                    .IsUnique();

                entity.Property(e => e.ScrId).HasColumnName("scr_id");

                entity.Property(e => e.ScrFecha)
                    .HasColumnName("scr_fecha")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ScrNombre)
                    .HasColumnName("scr_nombre")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ScrTag)
                    .IsRequired()
                    .HasColumnName("scr_tag")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ScrVersion).HasColumnName("scr_version");
            });

            modelBuilder.Entity<SecurityActions>(entity =>
            {
                entity.HasKey(e => e.ActId);

                entity.ToTable("security_actions");

                entity.HasIndex(e => e.ActName)
                    .HasName("IX_security_actions")
                    .IsUnique();

                entity.Property(e => e.ActId).HasColumnName("act_id");

                entity.Property(e => e.ActDescription)
                    .HasColumnName("act_description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ActMenId).HasColumnName("act_men_id");

                entity.Property(e => e.ActName)
                    .IsRequired()
                    .HasColumnName("act_name")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.ActMen)
                    .WithMany(p => p.SecurityActions)
                    .HasForeignKey(d => d.ActMenId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_security_actions_security_menus");
            });

            modelBuilder.Entity<SecurityMenus>(entity =>
            {
                entity.HasKey(e => e.MenId);

                entity.ToTable("security_menus");

                entity.Property(e => e.MenId).HasColumnName("men_id");

                entity.Property(e => e.MenChildClickHandler)
                    .HasColumnName("men_child_click_handler")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MenClickHandler)
                    .HasColumnName("men_click_handler")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MenDescription)
                    .IsRequired()
                    .HasColumnName("men_description")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MenHasSeparatorAfter).HasColumnName("men_has_separator_after");

                entity.Property(e => e.MenImage)
                    .HasColumnName("men_image")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MenName)
                    .IsRequired()
                    .HasColumnName("men_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MenOrder).HasColumnName("men_order");

                entity.Property(e => e.MenParentId).HasColumnName("men_parent_id");

                entity.HasOne(d => d.MenParent)
                    .WithMany(p => p.InverseMenParent)
                    .HasForeignKey(d => d.MenParentId)
                    .HasConstraintName("FK_security_menus_security_menus");
            });

            modelBuilder.Entity<SecurityProfiles>(entity =>
            {
                entity.HasKey(e => e.ProId);

                entity.ToTable("security_profiles");

                entity.Property(e => e.ProId).HasColumnName("pro_id");

                entity.Property(e => e.ProName)
                    .IsRequired()
                    .HasColumnName("pro_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SecurityProfilesActions>(entity =>
            {
                entity.HasKey(e => e.SpaId);

                entity.ToTable("security_profiles_actions");

                entity.Property(e => e.SpaId).HasColumnName("spa_id");

                entity.Property(e => e.SpaActId).HasColumnName("spa_act_id");

                entity.Property(e => e.SpaProId).HasColumnName("spa_pro_id");

                entity.HasOne(d => d.SpaAct)
                    .WithMany(p => p.SecurityProfilesActions)
                    .HasForeignKey(d => d.SpaActId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_security_profiles_actions_security_actions");

                entity.HasOne(d => d.SpaPro)
                    .WithMany(p => p.SecurityProfilesActions)
                    .HasForeignKey(d => d.SpaProId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_security_profiles_actions_security_profiles");
            });

            modelBuilder.Entity<SecurityProfilesUsers>(entity =>
            {
                entity.HasKey(e => e.SpuId);

                entity.ToTable("security_profiles_users");

                entity.Property(e => e.SpuId).HasColumnName("spu_id");

                entity.Property(e => e.SpuProId).HasColumnName("spu_pro_id");

                entity.Property(e => e.SpuUsrId).HasColumnName("spu_usr_id");

                entity.HasOne(d => d.SpuPro)
                    .WithMany(p => p.SecurityProfilesUsers)
                    .HasForeignKey(d => d.SpuProId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_security_profiles_users_security_profiles");

                entity.HasOne(d => d.SpuUsr)
                    .WithMany(p => p.SecurityProfilesUsers)
                    .HasForeignKey(d => d.SpuUsrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_security_profiles_users_security_users");
            });

            modelBuilder.Entity<SecurityUsers>(entity =>
            {
                entity.HasKey(e => e.UsrId);

                entity.ToTable("security_users");

                entity.Property(e => e.UsrId).HasColumnName("usr_id");

                entity.Property(e => e.UsrActive).HasColumnName("usr_active");

                entity.Property(e => e.UsrBlocked).HasColumnName("usr_blocked");

                entity.Property(e => e.UsrCreateDate)
                    .HasColumnName("usr_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UsrCreateUserId).HasColumnName("usr_create_user_id");

                entity.Property(e => e.UsrDeleteDate)
                    .HasColumnName("usr_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UsrDeleteUserId).HasColumnName("usr_delete_user_id");

                entity.Property(e => e.UsrDeleted).HasColumnName("usr_deleted");

                entity.Property(e => e.UsrFirstName)
                    .HasColumnName("usr_first_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsrLastName)
                    .HasColumnName("usr_last_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsrName)
                    .HasColumnName("usr_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsrPassword)
                    .HasColumnName("usr_password")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsrPwdExpire)
                    .HasColumnName("usr_pwd_expire")
                    .HasColumnType("datetime");

                entity.Property(e => e.UsrUpdateDate)
                    .HasColumnName("usr_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UsrUpdateUserId).HasColumnName("usr_update_user_id");
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.HasKey(e => e.SerId);

                entity.HasIndex(e => e.SerTag)
                    .HasName("IX_Service")
                    .IsUnique();

                entity.Property(e => e.SerId).HasColumnName("ser_id");

                entity.Property(e => e.SerAllowsCombo).HasColumnName("ser_allows_combo");

                entity.Property(e => e.SerDepartmentId).HasColumnName("ser_department_id");

                entity.Property(e => e.SerName)
                    .IsRequired()
                    .HasColumnName("ser_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerPhysicianSelection).HasColumnName("ser_physician_selection");

                entity.Property(e => e.SerPhysicianSelectionFirstScreen)
                    .IsRequired()
                    .HasColumnName("ser_physician_selection_first_screen")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SerRequiredPrescriptionDate).HasColumnName("ser_required_prescription_date");

                entity.Property(e => e.SerTag)
                    .IsRequired()
                    .HasColumnName("ser_tag")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.SerDepartment)
                    .WithMany(p => p.Service)
                    .HasForeignKey(d => d.SerDepartmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Service_Department");
            });

            modelBuilder.Entity<SourceDistribution>(entity =>
            {
                entity.HasKey(e => e.SodId);

                entity.ToTable("source_distribution");

                entity.Property(e => e.SodId).HasColumnName("sod_id");

                entity.Property(e => e.SodCode)
                    .IsRequired()
                    .HasColumnName("sod_code")
                    .HasMaxLength(20);

                entity.Property(e => e.SodOptionsQuantity).HasColumnName("sod_options_quantity");

                entity.Property(e => e.SodWeeksQuantity).HasColumnName("sod_weeks_quantity");
            });

            modelBuilder.Entity<State>(entity =>
            {
                entity.HasKey(e => e.StaId);

                entity.HasIndex(e => new { e.StaCountryId, e.StaName })
                    .HasName("IX_State")
                    .IsUnique();

                entity.Property(e => e.StaId).HasColumnName("sta_id");

                entity.Property(e => e.StaCountryId).HasColumnName("sta_country_id");

                entity.Property(e => e.StaName)
                    .HasColumnName("sta_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.StaCountry)
                    .WithMany(p => p.State)
                    .HasForeignKey(d => d.StaCountryId)
                    .HasConstraintName("FK_State_Country");
            });

            modelBuilder.Entity<TimeRange>(entity =>
            {
                entity.HasKey(e => e.TirId);

                entity.ToTable("time_range");

                entity.HasIndex(e => new { e.TirName, e.TirDeleteDate })
                    .HasName("IX_time_range")
                    .IsUnique();

                entity.Property(e => e.TirId).HasColumnName("tir_id");

                entity.Property(e => e.TirCreateDate)
                    .HasColumnName("tir_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.TirCreateUserId).HasColumnName("tir_create_user_id");

                entity.Property(e => e.TirDeleteDate)
                    .HasColumnName("tir_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.TirDeleteUserId).HasColumnName("tir_delete_user_id");

                entity.Property(e => e.TirEndTime)
                    .HasColumnName("tir_end_time")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TirMainOption).HasColumnName("tir_main_option");

                entity.Property(e => e.TirName)
                    .IsRequired()
                    .HasColumnName("tir_name")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TirStartTime)
                    .HasColumnName("tir_start_time")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TirUpdateDate)
                    .HasColumnName("tir_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.TirUpdateUserId).HasColumnName("tir_update_user_id");
            });

            modelBuilder.Entity<Translation>(entity =>
            {
                entity.HasKey(e => e.TraId);

                entity.ToTable("translation");

                entity.HasIndex(e => new { e.TraConnectionId, e.TraTranslationFieldId, e.TraTranslationTypeId, e.TraHealthcareCenterId })
                    .HasName("IX_translation")
                    .IsUnique();

                entity.Property(e => e.TraId).HasColumnName("tra_id");

                entity.Property(e => e.TraConnectionId).HasColumnName("tra_connection_id");

                entity.Property(e => e.TraHealthcareCenterId).HasColumnName("tra_healthcare_center_id");

                entity.Property(e => e.TraTranslationFieldId).HasColumnName("tra_translation_field_id");

                entity.Property(e => e.TraTranslationTypeId).HasColumnName("tra_translation_type_id");

                entity.HasOne(d => d.TraConnection)
                    .WithMany(p => p.Translation)
                    .HasForeignKey(d => d.TraConnectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_translation_cocnection");

                entity.HasOne(d => d.TraHealthcareCenter)
                    .WithMany(p => p.Translation)
                    .HasForeignKey(d => d.TraHealthcareCenterId)
                    .HasConstraintName("FK_translation_healthcare_center");

                entity.HasOne(d => d.TraTranslationField)
                    .WithMany(p => p.Translation)
                    .HasForeignKey(d => d.TraTranslationFieldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_translation_field");

                entity.HasOne(d => d.TraTranslationType)
                    .WithMany(p => p.Translation)
                    .HasForeignKey(d => d.TraTranslationTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_translation_type");
            });

            modelBuilder.Entity<TranslationField>(entity =>
            {
                entity.HasKey(e => e.TrfId);

                entity.ToTable("translation_field");

                entity.HasIndex(e => e.TrfName)
                    .HasName("IX_translation_field")
                    .IsUnique();

                entity.Property(e => e.TrfId).HasColumnName("trf_id");

                entity.Property(e => e.TrfName)
                    .IsRequired()
                    .HasColumnName("trf_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TranslationLog>(entity =>
            {
                entity.HasKey(e => e.TrlId);

                entity.ToTable("translation_log");

                entity.Property(e => e.TrlId).HasColumnName("trl_id");

                entity.Property(e => e.TrlCreateDate)
                    .HasColumnName("trl_create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrlCreateUserId).HasColumnName("trl_create_user_id");

                entity.Property(e => e.TrlDateLog)
                    .HasColumnName("trl_date_log")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrlDeleteDate)
                    .HasColumnName("trl_delete_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrlDeleteUserId).HasColumnName("trl_delete_user_id");

                entity.Property(e => e.TrlFieldName)
                    .IsRequired()
                    .HasColumnName("trl_field_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TrlOperationLogId).HasColumnName("trl_operation_log_id");

                entity.Property(e => e.TrlTranslationId).HasColumnName("trl_translation_id");

                entity.Property(e => e.TrlTranslationLogTypeId).HasColumnName("trl_translation_log_type_id");

                entity.Property(e => e.TrlUpdateDate)
                    .HasColumnName("trl_update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.TrlUpdateUserId).HasColumnName("trl_update_user_id");

                entity.Property(e => e.TrlValue)
                    .IsRequired()
                    .HasColumnName("trl_value")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TrlWsUrl)
                    .HasColumnName("trl_ws_url")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.HasOne(d => d.TrlOperationLog)
                    .WithMany(p => p.TranslationLog)
                    .HasForeignKey(d => d.TrlOperationLogId)
                    .HasConstraintName("FK_translation_log_operation_log");

                entity.HasOne(d => d.TrlTranslationLogType)
                    .WithMany(p => p.TranslationLog)
                    .HasForeignKey(d => d.TrlTranslationLogTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_translation_log_type_id");
            });

            modelBuilder.Entity<TranslationLogType>(entity =>
            {
                entity.HasKey(e => e.TltId);

                entity.ToTable("translation_log_type");

                entity.HasIndex(e => e.TltName)
                    .HasName("tlt_by_name")
                    .IsUnique();

                entity.Property(e => e.TltId).HasColumnName("tlt_id");

                entity.Property(e => e.TltName)
                    .IsRequired()
                    .HasColumnName("tlt_name")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TranslationType>(entity =>
            {
                entity.HasKey(e => e.TrtId);

                entity.ToTable("translation_type");

                entity.HasIndex(e => e.TrtName)
                    .HasName("trt_by_name")
                    .IsUnique();

                entity.Property(e => e.TrtId).HasColumnName("trt_id");

                entity.Property(e => e.TrtName)
                    .IsRequired()
                    .HasColumnName("trt_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TranslationValues>(entity =>
            {
                entity.HasKey(e => e.TrvId);

                entity.ToTable("translation_values");

                entity.HasIndex(e => new { e.TrvTranslationId, e.TrvInternalCode, e.TrvExternalCode })
                    .HasName("IX_translation_values")
                    .IsUnique();

                entity.Property(e => e.TrvId).HasColumnName("trv_id");

                entity.Property(e => e.TrvExternalCode)
                    .IsRequired()
                    .HasColumnName("trv_external_code")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TrvInternalCode)
                    .IsRequired()
                    .HasColumnName("trv_internal_code")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TrvTranslationId).HasColumnName("trv_translation_id");

                entity.HasOne(d => d.TrvTranslation)
                    .WithMany(p => p.TranslationValues)
                    .HasForeignKey(d => d.TrvTranslationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_translation_values");
            });

            modelBuilder.Entity<UserOrganization>(entity =>
            {
                entity.HasKey(e => e.UsoId);

                entity.ToTable("user_organization");

                entity.HasIndex(e => new { e.UsoUserId, e.UsoOrganizationId })
                    .HasName("IX_user_organization")
                    .IsUnique();

                entity.Property(e => e.UsoId).HasColumnName("uso_id");

                entity.Property(e => e.UsoOrganizationId).HasColumnName("uso_organization_id");

                entity.Property(e => e.UsoUserId).HasColumnName("uso_user_id");

                entity.HasOne(d => d.UsoUser)
                    .WithMany(p => p.UserOrganization)
                    .HasForeignKey(d => d.UsoUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_user_organization_security_users");
            });

            modelBuilder.Entity<WebServiceType>(entity =>
            {
                entity.HasKey(e => e.WstId);

                entity.ToTable("web_service_type");

                entity.HasIndex(e => e.WstName)
                    .HasName("IX_web_service_type")
                    .IsUnique();

                entity.Property(e => e.WstId).HasColumnName("wst_id");

                entity.Property(e => e.WstName)
                    .IsRequired()
                    .HasColumnName("wst_name")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
        }
    }
}
