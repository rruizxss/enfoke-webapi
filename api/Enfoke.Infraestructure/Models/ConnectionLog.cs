﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class ConnectionLog
    {
        public int ColId { get; set; }
        public int ColConnectionLogType { get; set; }
        public string ColWsUrl { get; set; }
        public string ColWsResult { get; set; }
        public int ColConnectionId { get; set; }
        public DateTime? ColDateLog { get; set; }
        public DateTime? ColCreateDate { get; set; }
        public DateTime? ColUpdateDate { get; set; }
        public DateTime? ColDeleteDate { get; set; }
        public int? ColCreateUserId { get; set; }
        public int? ColUpdateUserId { get; set; }
        public int? ColDeleteUserId { get; set; }
        public int? ColOperationLogId { get; set; }

        public virtual Connection ColConnection { get; set; }
        public virtual ConnectionLogType ColConnectionLogTypeNavigation { get; set; }
        public virtual OperationLog ColOperationLog { get; set; }
    }
}
