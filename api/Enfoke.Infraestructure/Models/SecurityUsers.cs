﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class SecurityUsers
    {
        public SecurityUsers()
        {
            ActivationToken = new HashSet<ActivationToken>();
            Appointment = new HashSet<Appointment>();
            InfoSettings = new HashSet<InfoSettings>();
            PreTranslationLog = new HashSet<PreTranslationLog>();
            SecurityProfilesUsers = new HashSet<SecurityProfilesUsers>();
            UserOrganization = new HashSet<UserOrganization>();
        }

        public int UsrId { get; set; }
        public string UsrPassword { get; set; }
        public string UsrFirstName { get; set; }
        public string UsrLastName { get; set; }
        public DateTime? UsrPwdExpire { get; set; }
        public bool UsrActive { get; set; }
        public string UsrName { get; set; }
        public bool? UsrDeleted { get; set; }
        public bool UsrBlocked { get; set; }
        public DateTime? UsrCreateDate { get; set; }
        public DateTime? UsrUpdateDate { get; set; }
        public DateTime? UsrDeleteDate { get; set; }
        public int? UsrCreateUserId { get; set; }
        public int? UsrUpdateUserId { get; set; }
        public int? UsrDeleteUserId { get; set; }

        public virtual ICollection<ActivationToken> ActivationToken { get; set; }
        public virtual ICollection<Appointment> Appointment { get; set; }
        public virtual ICollection<InfoSettings> InfoSettings { get; set; }
        public virtual ICollection<PreTranslationLog> PreTranslationLog { get; set; }
        public virtual ICollection<SecurityProfilesUsers> SecurityProfilesUsers { get; set; }
        public virtual ICollection<UserOrganization> UserOrganization { get; set; }
    }
}
