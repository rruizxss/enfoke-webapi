﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class MessageMobile
    {
        public int MebId { get; set; }
        public string MebTag { get; set; }
        public string MebScreenName { get; set; }
        public string MebMessage { get; set; }
        public bool MebShowOnLoad { get; set; }
    }
}
