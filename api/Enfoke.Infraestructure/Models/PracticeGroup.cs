﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class PracticeGroup
    {
        public PracticeGroup()
        {
            MessagePracticeGroup = new HashSet<MessagePracticeGroup>();
            PracticeGroupPractice = new HashSet<PracticeGroupPractice>();
            QuestionPracticeGroup = new HashSet<QuestionPracticeGroup>();
        }

        public int PrgId { get; set; }
        public string PrgName { get; set; }

        public virtual ICollection<MessagePracticeGroup> MessagePracticeGroup { get; set; }
        public virtual ICollection<PracticeGroupPractice> PracticeGroupPractice { get; set; }
        public virtual ICollection<QuestionPracticeGroup> QuestionPracticeGroup { get; set; }
    }
}
