﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class PracticeGroupPractice
    {
        public int PgpId { get; set; }
        public int PgpPracticeGroupId { get; set; }
        public int PgpPracticeId { get; set; }

        public virtual Practice PgpPractice { get; set; }
        public virtual PracticeGroup PgpPracticeGroup { get; set; }
    }
}
