﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class TranslationLog
    {
        public int TrlId { get; set; }
        public int TrlTranslationLogTypeId { get; set; }
        public string TrlWsUrl { get; set; }
        public int? TrlTranslationId { get; set; }
        public string TrlFieldName { get; set; }
        public string TrlValue { get; set; }
        public DateTime TrlDateLog { get; set; }
        public DateTime? TrlCreateDate { get; set; }
        public DateTime? TrlUpdateDate { get; set; }
        public DateTime? TrlDeleteDate { get; set; }
        public int? TrlCreateUserId { get; set; }
        public int? TrlUpdateUserId { get; set; }
        public int? TrlDeleteUserId { get; set; }
        public int? TrlOperationLogId { get; set; }

        public virtual OperationLog TrlOperationLog { get; set; }
        public virtual TranslationLogType TrlTranslationLogType { get; set; }
    }
}
