﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class InsurancePlan
    {
        public InsurancePlan()
        {
            InsurancePlanPatient = new HashSet<InsurancePlanPatient>();
            PreTranslationLog = new HashSet<PreTranslationLog>();
        }

        public int InpId { get; set; }
        public string InpCode { get; set; }
        public string InpName { get; set; }
        public int InpInsuranceId { get; set; }
        public DateTime InpCreateDate { get; set; }
        public DateTime? InpUpdateDate { get; set; }
        public DateTime? InpDeleteDate { get; set; }
        public int InpCreateUserId { get; set; }
        public int? InpUpdateUserId { get; set; }
        public int? InpDeleteUserId { get; set; }

        public virtual Insurance InpInsurance { get; set; }
        public virtual ICollection<InsurancePlanPatient> InsurancePlanPatient { get; set; }
        public virtual ICollection<PreTranslationLog> PreTranslationLog { get; set; }
    }
}
