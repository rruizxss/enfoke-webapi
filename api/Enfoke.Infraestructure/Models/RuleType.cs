﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class RuleType
    {
        public RuleType()
        {
            PreTranslationRule = new HashSet<PreTranslationRule>();
        }

        public int RtyId { get; set; }
        public string RtyName { get; set; }
        public int RtyApplicationOrder { get; set; }

        public virtual ICollection<PreTranslationRule> PreTranslationRule { get; set; }
    }
}
