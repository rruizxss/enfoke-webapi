﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class PracticeRestrictionType
    {
        public PracticeRestrictionType()
        {
            PracticeRestriction = new HashSet<PracticeRestriction>();
        }

        public int PrtId { get; set; }
        public string PrtName { get; set; }
        public string PrtMessageTag { get; set; }

        public virtual ICollection<PracticeRestriction> PracticeRestriction { get; set; }
    }
}
