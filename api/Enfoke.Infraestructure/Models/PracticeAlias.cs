﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class PracticeAlias
    {
        public int PalId { get; set; }
        public string PalName { get; set; }
        public bool PalMainId { get; set; }
        public int PalPracticeId { get; set; }

        public virtual Practice PalPractice { get; set; }
    }
}
