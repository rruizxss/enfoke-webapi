﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class PracticeFromPreTranslationRule
    {
        public int PfrId { get; set; }
        public int PfrRuleId { get; set; }
        public int PfrPracticeId { get; set; }

        public virtual Practice PfrPractice { get; set; }
        public virtual PreTranslationRule PfrRule { get; set; }
    }
}
