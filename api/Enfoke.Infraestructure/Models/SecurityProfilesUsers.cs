﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class SecurityProfilesUsers
    {
        public int SpuId { get; set; }
        public int SpuProId { get; set; }
        public int SpuUsrId { get; set; }

        public virtual SecurityProfiles SpuPro { get; set; }
        public virtual SecurityUsers SpuUsr { get; set; }
    }
}
