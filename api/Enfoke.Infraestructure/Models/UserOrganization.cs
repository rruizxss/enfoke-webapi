﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class UserOrganization
    {
        public int UsoId { get; set; }
        public int UsoUserId { get; set; }
        public int UsoOrganizationId { get; set; }

        public virtual SecurityUsers UsoUser { get; set; }
    }
}
