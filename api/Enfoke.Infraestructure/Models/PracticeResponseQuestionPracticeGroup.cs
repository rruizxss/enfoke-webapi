﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class PracticeResponseQuestionPracticeGroup
    {
        public int PrqId { get; set; }
        public int PrqResponseQuestionPracticeGroupId { get; set; }
        public int PrqPracticeAddedId { get; set; }

        public virtual Practice PrqPracticeAdded { get; set; }
        public virtual ResponseQuestionPracticeGroup PrqResponseQuestionPracticeGroup { get; set; }
    }
}
