﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class ForcedCombos
    {
        public int FcoId { get; set; }
        public string FcoPraNameA { get; set; }
        public string FcoPraNameB { get; set; }
    }
}
