﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class InsuranceAlias
    {
        public int IalId { get; set; }
        public int IalInsuranceId { get; set; }
        public string IalName { get; set; }
        public bool IalMain { get; set; }

        public virtual Insurance IalInsurance { get; set; }
    }
}
