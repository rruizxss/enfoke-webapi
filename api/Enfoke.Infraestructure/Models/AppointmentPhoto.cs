﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class AppointmentPhoto
    {
        public int AphId { get; set; }
        public int AphAppointmentExternalId { get; set; }
        public byte[] AphPhotoRaw { get; set; }
    }
}
