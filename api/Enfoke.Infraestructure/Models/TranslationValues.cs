﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class TranslationValues
    {
        public int TrvId { get; set; }
        public int TrvTranslationId { get; set; }
        public string TrvInternalCode { get; set; }
        public string TrvExternalCode { get; set; }

        public virtual Translation TrvTranslation { get; set; }
    }
}
