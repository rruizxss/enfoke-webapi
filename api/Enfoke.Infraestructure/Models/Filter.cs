﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Filter
    {
        public int FilId { get; set; }
        public string FilName { get; set; }
        public string FilTag { get; set; }
        public bool FilRefreshData { get; set; }
        public string FilDefaultValue { get; set; }
        public bool FilMainOption { get; set; }
        public DateTime FilCreateDate { get; set; }
        public DateTime? FilUpdateDate { get; set; }
        public DateTime? FilDeleteDate { get; set; }
        public int FilCreateUserId { get; set; }
        public int? FilUpdateUserId { get; set; }
        public int? FilDeleteUserId { get; set; }
    }
}
