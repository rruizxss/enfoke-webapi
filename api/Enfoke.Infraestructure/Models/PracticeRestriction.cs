﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class PracticeRestriction
    {
        public int PreId { get; set; }
        public int PrePracticeRestrictionTypeId { get; set; }
        public string PreItemId { get; set; }
        public int PrePracticeRestrictedId { get; set; }

        public virtual Practice PrePracticeRestricted { get; set; }
        public virtual PracticeRestrictionType PrePracticeRestrictionType { get; set; }
    }
}
