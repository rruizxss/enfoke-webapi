﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Connection
    {
        public Connection()
        {
            ConnectionLog = new HashSet<ConnectionLog>();
            Translation = new HashSet<Translation>();
        }

        public int ConId { get; set; }
        public int ConClientAppId { get; set; }
        public int ConOrganizationId { get; set; }
        public int ConDepartmentId { get; set; }
        public int ConWebServiceTypeId { get; set; }
        public string ConExternalAccessUrl { get; set; }
        public string ConPayloadInformation { get; set; }
        public string ConApplicationUrl { get; set; }
        public string ConValidationUrl { get; set; }
        public int? ConHealthcareCenterId { get; set; }
        public int? ConMaxoffers { get; set; }

        public virtual ClientApp ConClientApp { get; set; }
        public virtual Department ConDepartment { get; set; }
        public virtual HealthcareCenter ConHealthcareCenter { get; set; }
        public virtual Organization ConOrganization { get; set; }
        public virtual WebServiceType ConWebServiceType { get; set; }
        public virtual ICollection<ConnectionLog> ConnectionLog { get; set; }
        public virtual ICollection<Translation> Translation { get; set; }
    }
}
