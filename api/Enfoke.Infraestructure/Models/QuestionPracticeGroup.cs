﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class QuestionPracticeGroup
    {
        public QuestionPracticeGroup()
        {
            ResponseQuestionPracticeGroup = new HashSet<ResponseQuestionPracticeGroup>();
        }

        public int QpgId { get; set; }
        public int QpgQuestionId { get; set; }
        public int QpgPracticeGroupId { get; set; }
        public int? QpgOrder { get; set; }

        public virtual PracticeGroup QpgPracticeGroup { get; set; }
        public virtual Question QpgQuestion { get; set; }
        public virtual ICollection<ResponseQuestionPracticeGroup> ResponseQuestionPracticeGroup { get; set; }
    }
}
