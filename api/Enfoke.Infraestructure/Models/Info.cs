﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Info
    {
        public Info()
        {
            InfoSettings = new HashSet<InfoSettings>();
        }

        public int InfId { get; set; }
        public string InfDiscriminator { get; set; }

        public virtual ICollection<InfoSettings> InfoSettings { get; set; }
    }
}
