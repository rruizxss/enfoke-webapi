﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Service
    {
        public Service()
        {
            Practice = new HashSet<Practice>();
            PreTranslationRule = new HashSet<PreTranslationRule>();
            RepresentativeService = new HashSet<RepresentativeService>();
        }

        public int SerId { get; set; }
        public string SerTag { get; set; }
        public string SerName { get; set; }
        public int SerDepartmentId { get; set; }
        public bool SerPhysicianSelection { get; set; }
        public bool? SerPhysicianSelectionFirstScreen { get; set; }
        public bool SerAllowsCombo { get; set; }
        public bool SerRequiredPrescriptionDate { get; set; }

        public virtual Department SerDepartment { get; set; }
        public virtual ICollection<Practice> Practice { get; set; }
        public virtual ICollection<PreTranslationRule> PreTranslationRule { get; set; }
        public virtual ICollection<RepresentativeService> RepresentativeService { get; set; }
    }
}
