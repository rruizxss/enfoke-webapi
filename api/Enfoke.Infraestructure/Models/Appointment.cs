﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Appointment
    {
        public int AppId { get; set; }
        public int AppConnectionId { get; set; }
        public string AppExternalId { get; set; }
        public int AppCreateUserId { get; set; }
        public DateTime AppCreateDate { get; set; }
        public int? AppExternalComboId { get; set; }

        public virtual SecurityUsers AppCreateUser { get; set; }
    }
}
