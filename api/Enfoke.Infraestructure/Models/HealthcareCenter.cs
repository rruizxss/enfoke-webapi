﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class HealthcareCenter
    {
        public HealthcareCenter()
        {
            Connection = new HashSet<Connection>();
            RepresentativeHealthCareCenter = new HashSet<RepresentativeHealthCareCenter>();
            Translation = new HashSet<Translation>();
        }

        public int HecId { get; set; }
        public string HecTag { get; set; }
        public string HecName { get; set; }
        public int HecOrganizationId { get; set; }
        public int? HecAddressCityId { get; set; }
        public int? HecAddressProvinceId { get; set; }
        public int? HecAddressCountryId { get; set; }
        public string HecAddressStreet { get; set; }
        public string HecAddressNumber { get; set; }
        public string HecAddressLocationCode { get; set; }
        public DateTime HecCreateDate { get; set; }
        public DateTime? HecUpdateDate { get; set; }
        public DateTime? HecDeleteDate { get; set; }
        public int HecCreateUserId { get; set; }
        public int? HecUpdateUserId { get; set; }
        public int? HecDeleteUserId { get; set; }
        public string HecContactNumber { get; set; }
        public string HecContactMail { get; set; }

        public virtual City HecAddressCity { get; set; }
        public virtual Country HecAddressCountry { get; set; }
        public virtual State HecAddressProvince { get; set; }
        public virtual Organization HecOrganization { get; set; }
        public virtual ICollection<Connection> Connection { get; set; }
        public virtual ICollection<RepresentativeHealthCareCenter> RepresentativeHealthCareCenter { get; set; }
        public virtual ICollection<Translation> Translation { get; set; }
    }
}
