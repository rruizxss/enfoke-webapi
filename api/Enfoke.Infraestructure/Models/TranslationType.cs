﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class TranslationType
    {
        public TranslationType()
        {
            Translation = new HashSet<Translation>();
        }

        public int TrtId { get; set; }
        public string TrtName { get; set; }

        public virtual ICollection<Translation> Translation { get; set; }
    }
}
