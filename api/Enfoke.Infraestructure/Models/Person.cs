﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Person
    {
        public int PerId { get; set; }
        public string PerName { get; set; }
        public string PerSurname { get; set; }
    }
}
