﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class SecurityProfilesActions
    {
        public int SpaId { get; set; }
        public int SpaActId { get; set; }
        public int SpaProId { get; set; }

        public virtual SecurityActions SpaAct { get; set; }
        public virtual SecurityProfiles SpaPro { get; set; }
    }
}
