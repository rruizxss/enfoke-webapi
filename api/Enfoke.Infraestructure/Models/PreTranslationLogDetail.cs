﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class PreTranslationLogDetail
    {
        public int PldId { get; set; }
        public int PldLogId { get; set; }
        public int PldRuleAppliedId { get; set; }

        public virtual PreTranslationLog PldLog { get; set; }
    }
}
