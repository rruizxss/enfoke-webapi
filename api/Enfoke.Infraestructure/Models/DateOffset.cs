﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class DateOffset
    {
        public int DaoId { get; set; }
        public string DaoName { get; set; }
        public int? DaoOffsetFrom { get; set; }
        public bool DaoDefault { get; set; }
        public DateTime DaoCreateDate { get; set; }
        public DateTime? DaoUpdateDate { get; set; }
        public DateTime? DaoDeleteDate { get; set; }
        public int DaoCreateUserId { get; set; }
        public int? DaoUpdateUserId { get; set; }
        public int? DaoDeleteUserId { get; set; }
        public int? DaoOffsetTo { get; set; }
        public string DaoType { get; set; }
    }
}
