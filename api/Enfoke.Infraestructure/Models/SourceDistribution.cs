﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class SourceDistribution
    {
        public int SodId { get; set; }
        public string SodCode { get; set; }
        public int SodOptionsQuantity { get; set; }
        public int SodWeeksQuantity { get; set; }
    }
}
