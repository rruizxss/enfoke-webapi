﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class EnrollmentType
    {
        public int EntId { get; set; }
        public string EntTag { get; set; }
        public string EntName { get; set; }
    }
}
