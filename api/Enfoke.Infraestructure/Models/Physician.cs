﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Physician
    {
        public int PhyId { get; set; }
        public int PhyPatientId { get; set; }
        public string PhyMail { get; set; }
        public string PhyProvincialEnrollment { get; set; }
        public string PhyNationalEnrollment { get; set; }
        public string PhySpecialismEnrollment { get; set; }
        public string PhyPhone { get; set; }

        public virtual Patient PhyPatient { get; set; }
    }
}
