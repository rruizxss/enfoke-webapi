﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Department
    {
        public Department()
        {
            Connection = new HashSet<Connection>();
            Service = new HashSet<Service>();
        }

        public int DepId { get; set; }
        public string DepName { get; set; }

        public virtual ICollection<Connection> Connection { get; set; }
        public virtual ICollection<Service> Service { get; set; }
    }
}
