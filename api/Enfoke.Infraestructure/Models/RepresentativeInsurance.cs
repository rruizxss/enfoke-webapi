﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class RepresentativeInsurance
    {
        public int RinId { get; set; }
        public int RinRepresentativeId { get; set; }
        public int RinInsuranceId { get; set; }

        public virtual Insurance RinInsurance { get; set; }
        public virtual Representative RinRepresentative { get; set; }
    }
}
