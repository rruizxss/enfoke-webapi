﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class RepresentativeHealthCareCenter
    {
        public int RhcId { get; set; }
        public int RhcRepresentativeId { get; set; }
        public int RhcHealthCareCenterId { get; set; }

        public virtual HealthcareCenter RhcHealthCareCenter { get; set; }
        public virtual Representative RhcRepresentative { get; set; }
    }
}
