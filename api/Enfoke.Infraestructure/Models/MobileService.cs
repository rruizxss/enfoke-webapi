﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class MobileService
    {
        public int MsrId { get; set; }
        public string MsrTitle { get; set; }
        public string MsrDescriptionHtml { get; set; }
        public string MsrIconRelativePath { get; set; }
        public string MsrImageRelativePath { get; set; }
        public DateTime MsrCreateDate { get; set; }
        public int MsrCreateUserId { get; set; }
        public DateTime? MsrDeleteDate { get; set; }
        public int? MsrDeleteUserId { get; set; }
    }
}
