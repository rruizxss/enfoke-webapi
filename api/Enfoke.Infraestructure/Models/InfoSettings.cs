﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class InfoSettings
    {
        public int InsId { get; set; }
        public string InsKey { get; set; }
        public string InsDescription { get; set; }
        public string InsValue { get; set; }
        public int InsInfoId { get; set; }
        public DateTime? InsUpdateDate { get; set; }
        public int? InsUpdateUserId { get; set; }

        public virtual Info InsInfo { get; set; }
        public virtual SecurityUsers InsUpdateUser { get; set; }
    }
}
