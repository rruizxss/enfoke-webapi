﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class OffersDistribution
    {
        public int OfdId { get; set; }
        public decimal OfdPercentage { get; set; }
        public int OfdOffersAmount { get; set; }
        public int OfdOrder { get; set; }
    }
}
