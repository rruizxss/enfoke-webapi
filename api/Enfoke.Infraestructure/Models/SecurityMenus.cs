﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class SecurityMenus
    {
        public SecurityMenus()
        {
            InverseMenParent = new HashSet<SecurityMenus>();
            SecurityActions = new HashSet<SecurityActions>();
        }

        public int MenId { get; set; }
        public string MenName { get; set; }
        public string MenDescription { get; set; }
        public string MenImage { get; set; }
        public int? MenParentId { get; set; }
        public int MenOrder { get; set; }
        public string MenClickHandler { get; set; }
        public string MenChildClickHandler { get; set; }
        public bool MenHasSeparatorAfter { get; set; }

        public virtual SecurityMenus MenParent { get; set; }
        public virtual ICollection<SecurityMenus> InverseMenParent { get; set; }
        public virtual ICollection<SecurityActions> SecurityActions { get; set; }
    }
}
