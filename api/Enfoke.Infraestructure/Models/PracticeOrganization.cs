﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class PracticeOrganization
    {
        public int ProId { get; set; }
        public int ProPracticeId { get; set; }
        public int ProOrganizationId { get; set; }
        public int ProReservationTypeId { get; set; }
        public DateTime ProCreateDate { get; set; }
        public DateTime? ProUpdateDate { get; set; }
        public DateTime? ProDeleteDate { get; set; }
        public int ProCreateUserId { get; set; }
        public int? ProUpdateUserId { get; set; }
        public int? ProDeleteUserId { get; set; }

        public virtual Organization ProOrganization { get; set; }
        public virtual Practice ProPractice { get; set; }
        public virtual ReservationType ProReservationType { get; set; }
    }
}
