﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class PreTranslationRule
    {
        public PreTranslationRule()
        {
            PracticeFromPreTranslationRule = new HashSet<PracticeFromPreTranslationRule>();
            PracticeToPreTranslationRule = new HashSet<PracticeToPreTranslationRule>();
        }

        public int PtrId { get; set; }
        public string PtrName { get; set; }
        public int PtrRuleTypeId { get; set; }
        public string PtrObservations { get; set; }
        public bool PtrOnlyExactGroup { get; set; }
        public int? PtrInsuranceId { get; set; }
        public bool PtrExcludesInsurance { get; set; }
        public int? PtrServiceId { get; set; }
        public int? PtrOrganizationId { get; set; }
        public DateTime PtrCreateDate { get; set; }
        public DateTime? PtrDeleteDate { get; set; }
        public string PtrAgeCondition { get; set; }
        public bool? PtrApplyPartialMatch { get; set; }

        public virtual Insurance PtrInsurance { get; set; }
        public virtual Organization PtrOrganization { get; set; }
        public virtual RuleType PtrRuleType { get; set; }
        public virtual Service PtrService { get; set; }
        public virtual ICollection<PracticeFromPreTranslationRule> PracticeFromPreTranslationRule { get; set; }
        public virtual ICollection<PracticeToPreTranslationRule> PracticeToPreTranslationRule { get; set; }
    }
}
