﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class ClientApp
    {
        public ClientApp()
        {
            Connection = new HashSet<Connection>();
        }

        public int ClaId { get; set; }
        public string ClaName { get; set; }

        public virtual ICollection<Connection> Connection { get; set; }
    }
}
