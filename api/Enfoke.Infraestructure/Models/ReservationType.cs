﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class ReservationType
    {
        public ReservationType()
        {
            PracticeOrganization = new HashSet<PracticeOrganization>();
        }

        public int RetId { get; set; }
        public string RetTag { get; set; }
        public string RetName { get; set; }
        public int RetPriority { get; set; }

        public virtual ICollection<PracticeOrganization> PracticeOrganization { get; set; }
    }
}
