﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Message
    {
        public Message()
        {
            MessagePracticeGroup = new HashSet<MessagePracticeGroup>();
        }

        public int MesId { get; set; }
        public string MesTag { get; set; }
        public string MesValue { get; set; }

        public virtual ICollection<MessagePracticeGroup> MessagePracticeGroup { get; set; }
    }
}
