﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Country
    {
        public Country()
        {
            HealthcareCenter = new HashSet<HealthcareCenter>();
            Patient = new HashSet<Patient>();
            State = new HashSet<State>();
        }

        public int CouId { get; set; }
        public string CouName { get; set; }

        public virtual ICollection<HealthcareCenter> HealthcareCenter { get; set; }
        public virtual ICollection<Patient> Patient { get; set; }
        public virtual ICollection<State> State { get; set; }
    }
}
