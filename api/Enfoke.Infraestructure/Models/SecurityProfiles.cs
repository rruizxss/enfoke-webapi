﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class SecurityProfiles
    {
        public SecurityProfiles()
        {
            SecurityProfilesActions = new HashSet<SecurityProfilesActions>();
            SecurityProfilesUsers = new HashSet<SecurityProfilesUsers>();
        }

        public int ProId { get; set; }
        public string ProName { get; set; }

        public virtual ICollection<SecurityProfilesActions> SecurityProfilesActions { get; set; }
        public virtual ICollection<SecurityProfilesUsers> SecurityProfilesUsers { get; set; }
    }
}
