﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Script
    {
        public int ScrId { get; set; }
        public string ScrTag { get; set; }
        public string ScrNombre { get; set; }
        public int? ScrVersion { get; set; }
        public DateTime ScrFecha { get; set; }
    }
}
