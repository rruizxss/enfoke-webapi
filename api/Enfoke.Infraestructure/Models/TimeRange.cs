﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class TimeRange
    {
        public int TirId { get; set; }
        public string TirName { get; set; }
        public decimal TirStartTime { get; set; }
        public decimal TirEndTime { get; set; }
        public bool TirMainOption { get; set; }
        public DateTime TirCreateDate { get; set; }
        public DateTime? TirUpdateDate { get; set; }
        public DateTime? TirDeleteDate { get; set; }
        public int TirCreateUserId { get; set; }
        public int? TirUpdateUserId { get; set; }
        public int? TirDeleteUserId { get; set; }
    }
}
