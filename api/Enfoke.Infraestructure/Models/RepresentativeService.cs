﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class RepresentativeService
    {
        public int ResId { get; set; }
        public int ResRepresentativeId { get; set; }
        public int ResServiceId { get; set; }

        public virtual Representative ResRepresentative { get; set; }
        public virtual Service ResService { get; set; }
    }
}
