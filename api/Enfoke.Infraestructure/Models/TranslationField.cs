﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class TranslationField
    {
        public TranslationField()
        {
            Translation = new HashSet<Translation>();
        }

        public int TrfId { get; set; }
        public string TrfName { get; set; }

        public virtual ICollection<Translation> Translation { get; set; }
    }
}
