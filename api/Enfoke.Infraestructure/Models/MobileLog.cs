﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class MobileLog
    {
        public int MolId { get; set; }
        public DateTime MolDatetime { get; set; }
        public int? MolMobileSessionId { get; set; }
        public string MolDetail { get; set; }
        public string MolPayload { get; set; }
    }
}
