﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class InsurancePlanPatient
    {
        public int IppId { get; set; }
        public int IppPatientId { get; set; }
        public int IppInsaurancePlanId { get; set; }
        public bool IppDefault { get; set; }
        public DateTime IppCreateDate { get; set; }
        public DateTime? IppUpdateDate { get; set; }
        public DateTime? IppDeleteDate { get; set; }
        public int IppCreateUserId { get; set; }
        public int? IppUpdateUserId { get; set; }
        public int? IppDeleteUserId { get; set; }

        public virtual InsurancePlan IppInsaurancePlan { get; set; }
        public virtual Patient IppPatient { get; set; }
    }
}
