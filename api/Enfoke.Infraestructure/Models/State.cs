﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class State
    {
        public State()
        {
            City = new HashSet<City>();
            HealthcareCenter = new HashSet<HealthcareCenter>();
            Patient = new HashSet<Patient>();
        }

        public int StaId { get; set; }
        public string StaName { get; set; }
        public int? StaCountryId { get; set; }

        public virtual Country StaCountry { get; set; }
        public virtual ICollection<City> City { get; set; }
        public virtual ICollection<HealthcareCenter> HealthcareCenter { get; set; }
        public virtual ICollection<Patient> Patient { get; set; }
    }
}
