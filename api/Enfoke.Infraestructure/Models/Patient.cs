﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Patient
    {
        public Patient()
        {
            InsurancePlanPatient = new HashSet<InsurancePlanPatient>();
            PreTranslationLog = new HashSet<PreTranslationLog>();
            Representative = new HashSet<Representative>();
        }

        public int PatId { get; set; }
        public string PatFirstName { get; set; }
        public string PatLastName { get; set; }
        public int PatIdentificationTypeId { get; set; }
        public string PatPhoneNumber1 { get; set; }
        public string PatPhoneNumber2 { get; set; }
        public string PatZipcode { get; set; }
        public int? PatAddressCityId { get; set; }
        public int? PatAddressStateId { get; set; }
        public int? PatAddressCountryId { get; set; }
        public string PatAddressStreet { get; set; }
        public string PatAddressNumber { get; set; }
        public string PatAddressLocationCode { get; set; }
        public DateTime? PatUpdateDate { get; set; }
        public DateTime? PatDeleteDate { get; set; }
        public int? PatUpdateUserId { get; set; }
        public int? PatDeleteUserId { get; set; }
        public string PatSex { get; set; }
        public int? PatIdentificactionNumber { get; set; }
        public DateTime? PatDateOfBirth { get; set; }
        public DateTime? PatCreateDate { get; set; }
        public int? PatCreateUserId { get; set; }
        public string PatPhoneNumber3 { get; set; }
        public string PatMail { get; set; }
        public string PatPhoneNumber1Area { get; set; }
        public bool PatClaustrophobic { get; set; }
        public bool? PatRequiresUpdate { get; set; }

        public virtual City PatAddressCity { get; set; }
        public virtual Country PatAddressCountry { get; set; }
        public virtual State PatAddressState { get; set; }
        public virtual IdentificationType PatIdentificationType { get; set; }
        public virtual Operator Operator { get; set; }
        public virtual Physician Physician { get; set; }
        public virtual ICollection<InsurancePlanPatient> InsurancePlanPatient { get; set; }
        public virtual ICollection<PreTranslationLog> PreTranslationLog { get; set; }
        public virtual ICollection<Representative> Representative { get; set; }
    }
}
