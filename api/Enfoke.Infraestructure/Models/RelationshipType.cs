﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class RelationshipType
    {
        public int RetId { get; set; }
        public string RetName { get; set; }
    }
}
