﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class OperationLog
    {
        public OperationLog()
        {
            ConnectionLog = new HashSet<ConnectionLog>();
            TranslationLog = new HashSet<TranslationLog>();
        }

        public int OplId { get; set; }
        public int OplUserId { get; set; }
        public int? OplPatientId { get; set; }
        public int OplOperationId { get; set; }
        public DateTime OplDate { get; set; }
        public DateTime? OplCreateDate { get; set; }
        public DateTime? OplUpdateDate { get; set; }
        public DateTime? OplDeleteDate { get; set; }
        public int? OplCreateUserId { get; set; }
        public int? OplUpdateUserId { get; set; }
        public int? OplDeleteUserId { get; set; }
        public string OplAditionalInformation { get; set; }
        public string OplOrigin { get; set; }

        public virtual ICollection<ConnectionLog> ConnectionLog { get; set; }
        public virtual ICollection<TranslationLog> TranslationLog { get; set; }
    }
}
