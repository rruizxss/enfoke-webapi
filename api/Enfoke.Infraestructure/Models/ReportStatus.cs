﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class ReportStatus
    {
        public int ResId { get; set; }
        public string ResTag { get; set; }
        public string ResName { get; set; }
    }
}
