﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Practice
    {
        public Practice()
        {
            InversePraPracticeContrast = new HashSet<Practice>();
            InversePraPracticeSubsequent = new HashSet<Practice>();
            PracticeAlias = new HashSet<PracticeAlias>();
            PracticeFromPreTranslationRule = new HashSet<PracticeFromPreTranslationRule>();
            PracticeGroupPractice = new HashSet<PracticeGroupPractice>();
            PracticeOrganization = new HashSet<PracticeOrganization>();
            PracticeResponseQuestionPracticeGroup = new HashSet<PracticeResponseQuestionPracticeGroup>();
            PracticeRestriction = new HashSet<PracticeRestriction>();
            PracticeToPreTranslationRule = new HashSet<PracticeToPreTranslationRule>();
        }

        public int PraId { get; set; }
        public string PraCode { get; set; }
        public string PraName { get; set; }
        public int PraServiceId { get; set; }
        public DateTime PraCreateDate { get; set; }
        public DateTime? PraUpdateDate { get; set; }
        public DateTime? PraDeleteDate { get; set; }
        public int PraCreateUserId { get; set; }
        public int? PraUpdateUserId { get; set; }
        public int? PraDeleteUserId { get; set; }
        public bool PraIsHidden { get; set; }
        public int? PraPracticeContrastId { get; set; }
        public int? PraPracticeSubsequentId { get; set; }
        public int? PraRanking { get; set; }

        public virtual Practice PraPracticeContrast { get; set; }
        public virtual Practice PraPracticeSubsequent { get; set; }
        public virtual Service PraService { get; set; }
        public virtual ICollection<Practice> InversePraPracticeContrast { get; set; }
        public virtual ICollection<Practice> InversePraPracticeSubsequent { get; set; }
        public virtual ICollection<PracticeAlias> PracticeAlias { get; set; }
        public virtual ICollection<PracticeFromPreTranslationRule> PracticeFromPreTranslationRule { get; set; }
        public virtual ICollection<PracticeGroupPractice> PracticeGroupPractice { get; set; }
        public virtual ICollection<PracticeOrganization> PracticeOrganization { get; set; }
        public virtual ICollection<PracticeResponseQuestionPracticeGroup> PracticeResponseQuestionPracticeGroup { get; set; }
        public virtual ICollection<PracticeRestriction> PracticeRestriction { get; set; }
        public virtual ICollection<PracticeToPreTranslationRule> PracticeToPreTranslationRule { get; set; }
    }
}
