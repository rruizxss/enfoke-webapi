﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class TranslationLogType
    {
        public TranslationLogType()
        {
            TranslationLog = new HashSet<TranslationLog>();
        }

        public int TltId { get; set; }
        public string TltName { get; set; }

        public virtual ICollection<TranslationLog> TranslationLog { get; set; }
    }
}
