﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Operator
    {
        public int OpeId { get; set; }
        public int OpePatientId { get; set; }
        public string OpeMail { get; set; }
        public int OpeOrganizationId { get; set; }
        public string OpeLaboralIdentification { get; set; }

        public virtual Patient OpePatient { get; set; }
    }
}
