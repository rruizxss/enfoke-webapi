﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class MessagePracticeGroup
    {
        public int MpgId { get; set; }
        public int MpgMessageId { get; set; }
        public int MpgPracticeGroupId { get; set; }
        public int? MpgOrder { get; set; }

        public virtual Message MpgMessage { get; set; }
        public virtual PracticeGroup MpgPracticeGroup { get; set; }
    }
}
