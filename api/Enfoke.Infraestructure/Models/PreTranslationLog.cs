﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class PreTranslationLog
    {
        public PreTranslationLog()
        {
            PreTranslationLogDetail = new HashSet<PreTranslationLogDetail>();
        }

        public int PtlId { get; set; }
        public DateTime PtlCreationDate { get; set; }
        public int PtlCreationUserId { get; set; }
        public int PtlInsurancePlanId { get; set; }
        public int PtlOrganizationId { get; set; }
        public int PtlPatientId { get; set; }
        public string PtlPracticesFromIds { get; set; }
        public string PtlObservations { get; set; }
        public string PtlPracticesToIds { get; set; }
        public DateTime? PtlUpdateDate { get; set; }

        public virtual SecurityUsers PtlCreationUser { get; set; }
        public virtual InsurancePlan PtlInsurancePlan { get; set; }
        public virtual Organization PtlOrganization { get; set; }
        public virtual Patient PtlPatient { get; set; }
        public virtual ICollection<PreTranslationLogDetail> PreTranslationLogDetail { get; set; }
    }
}
