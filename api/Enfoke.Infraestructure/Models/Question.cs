﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Question
    {
        public Question()
        {
            QuestionPracticeGroup = new HashSet<QuestionPracticeGroup>();
        }

        public int QueId { get; set; }
        public string QueName { get; set; }

        public virtual ICollection<QuestionPracticeGroup> QuestionPracticeGroup { get; set; }
    }
}
