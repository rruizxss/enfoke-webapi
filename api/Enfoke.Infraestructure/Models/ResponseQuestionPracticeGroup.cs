﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class ResponseQuestionPracticeGroup
    {
        public ResponseQuestionPracticeGroup()
        {
            PracticeResponseQuestionPracticeGroup = new HashSet<PracticeResponseQuestionPracticeGroup>();
        }

        public int RqpId { get; set; }
        public int RqpQuestionPracticeGroupId { get; set; }
        public string RqpResponseText { get; set; }
        public bool? RqpForcedClaustrophobicCondition { get; set; }

        public virtual QuestionPracticeGroup RqpQuestionPracticeGroup { get; set; }
        public virtual ICollection<PracticeResponseQuestionPracticeGroup> PracticeResponseQuestionPracticeGroup { get; set; }
    }
}
