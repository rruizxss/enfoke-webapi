﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class PracticeToPreTranslationRule
    {
        public int PtrId { get; set; }
        public int PtrRuleId { get; set; }
        public int PtrPracticeId { get; set; }

        public virtual Practice PtrPractice { get; set; }
        public virtual PreTranslationRule PtrRule { get; set; }
    }
}
