﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class ConnectionLogType
    {
        public ConnectionLogType()
        {
            ConnectionLog = new HashSet<ConnectionLog>();
        }

        public int CltId { get; set; }
        public string CltName { get; set; }

        public virtual ICollection<ConnectionLog> ConnectionLog { get; set; }
    }
}
