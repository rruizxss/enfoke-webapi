﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Gender
    {
        public int GenId { get; set; }
        public string GenName { get; set; }
        public string GenOldSex { get; set; }
    }
}
