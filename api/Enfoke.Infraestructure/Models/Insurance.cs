﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Insurance
    {
        public Insurance()
        {
            InsuranceAlias = new HashSet<InsuranceAlias>();
            InsurancePlan = new HashSet<InsurancePlan>();
            PreTranslationRule = new HashSet<PreTranslationRule>();
            RepresentativeInsurance = new HashSet<RepresentativeInsurance>();
        }

        public int InsId { get; set; }
        public string InsCode { get; set; }
        public string InsName { get; set; }
        public bool InsPrivate { get; set; }
        public DateTime InsCreateDate { get; set; }
        public DateTime? InsUpdateDate { get; set; }
        public DateTime? InsDeleteDate { get; set; }
        public int InsCreateUserId { get; set; }
        public int? InsUpdateUserId { get; set; }
        public int? InsDeleteUserId { get; set; }

        public virtual ICollection<InsuranceAlias> InsuranceAlias { get; set; }
        public virtual ICollection<InsurancePlan> InsurancePlan { get; set; }
        public virtual ICollection<PreTranslationRule> PreTranslationRule { get; set; }
        public virtual ICollection<RepresentativeInsurance> RepresentativeInsurance { get; set; }
    }
}
