﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class ActivationToken
    {
        public int ActId { get; set; }
        public int? ActUserId { get; set; }
        public string ActToken { get; set; }
        public DateTime? ActDueDate { get; set; }
        public string ActDiscriminator { get; set; }
        public DateTime? ActCreateDate { get; set; }
        public DateTime? ActUpdateDate { get; set; }
        public DateTime? ActDeleteDate { get; set; }
        public int? ActCreateUserId { get; set; }
        public int? ActUpdateUserId { get; set; }
        public int? ActDeleteUserId { get; set; }

        public virtual SecurityUsers ActUser { get; set; }
    }
}
