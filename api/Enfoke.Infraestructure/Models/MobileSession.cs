﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class MobileSession
    {
        public int MosId { get; set; }
        public int? MosUserId { get; set; }
        public string MosToken { get; set; }
        public string MosIpAddress { get; set; }
        public DateTime MosExpirationDate { get; set; }
        public DateTime MosCreateDate { get; set; }
        public int MosCreateUserId { get; set; }
        public DateTime? MosDeleteDate { get; set; }
        public int? MosDeleteUserId { get; set; }
        public string MosPushToken { get; set; }
        public string MosUserDni { get; set; }
        public string MosDeviceOs { get; set; }
    }
}
