﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class WebServiceType
    {
        public WebServiceType()
        {
            Connection = new HashSet<Connection>();
        }

        public int WstId { get; set; }
        public string WstName { get; set; }

        public virtual ICollection<Connection> Connection { get; set; }
    }
}
