﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class IdentificationType
    {
        public IdentificationType()
        {
            Patient = new HashSet<Patient>();
        }

        public int IdtId { get; set; }
        public string IdtName { get; set; }

        public virtual ICollection<Patient> Patient { get; set; }
    }
}
