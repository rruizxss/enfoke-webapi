﻿using System;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Models
{
    public partial class Translation
    {
        public Translation()
        {
            TranslationValues = new HashSet<TranslationValues>();
        }

        public int TraId { get; set; }
        public int TraConnectionId { get; set; }
        public int TraTranslationFieldId { get; set; }
        public int TraTranslationTypeId { get; set; }
        public int? TraHealthcareCenterId { get; set; }

        public virtual Connection TraConnection { get; set; }
        public virtual HealthcareCenter TraHealthcareCenter { get; set; }
        public virtual TranslationField TraTranslationField { get; set; }
        public virtual TranslationType TraTranslationType { get; set; }
        public virtual ICollection<TranslationValues> TranslationValues { get; set; }
    }
}
