﻿using Enfoke.Infraestructure.Models;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Repositories.Interfaces
{
    public interface IPracticeAliasRepository
    {
        IEnumerable<PracticeAlias> GetAll();

        PracticeAlias GetBy(int id);

        void Update(PracticeAlias practice);

        void Put(PracticeAlias practice);

        void Delete(int id);
    }
}
