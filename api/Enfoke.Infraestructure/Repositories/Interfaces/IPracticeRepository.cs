﻿using Enfoke.Infraestructure.Models;
using System.Collections.Generic;

namespace Enfoke.Infraestructure.Repositories.Interfaces
{
    public interface IPracticeRepository
    {
        IEnumerable<Practice> GetAll();

        IEnumerable<Practice> GetAllWithAliases();

        Practice GetBy(int id);

        void Update(Practice practice);

        void Put(Practice practice);

        void Delete(int id);
    }
}
