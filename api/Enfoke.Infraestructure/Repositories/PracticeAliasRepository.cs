﻿using Enfoke.Infraestructure.Models;
using Enfoke.Infraestructure.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Enfoke.Infraestructure.Repositories
{
    public class PracticeAliasRepository : Repository, IPracticeAliasRepository
    {
        public PracticeAliasRepository() : base()
        {
        }

        public IEnumerable<PracticeAlias> GetAll()
        {
            return Context.PracticeAlias.AsEnumerable();
        }

        public PracticeAlias GetBy(int id)
        {
            return Context.PracticeAlias.FirstOrDefault(p => p.PalId == id);
        }

        public void Put(PracticeAlias practiceAlias)
        {
            Context.PracticeAlias.Add(practiceAlias);

            Context.SaveChanges();
        }

        public void Update(PracticeAlias practiceAlias)
        {
            Context.PracticeAlias.Update(practiceAlias);

            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var practiceAlias = new PracticeAlias() { PalId = id };
            
            Context.PracticeAlias.Attach(practiceAlias);

            Context.PracticeAlias.Remove(practiceAlias);
                
            Context.SaveChanges();
        }
    }
}
