﻿using Enfoke.Infraestructure.Models;

namespace Enfoke.Infraestructure.Repositories
{
    public class Repository
    {
        protected readonly MiSalud_27_MobileContext Context;

        public Repository()
        {
            Context = new MiSalud_27_MobileContext();
        }
    }
}
