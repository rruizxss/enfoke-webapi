﻿using Enfoke.Infraestructure.Models;
using Enfoke.Infraestructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Enfoke.Infraestructure.Repositories
{
    public class PracticeRepository : Repository, IPracticeRepository
    {
        public PracticeRepository() : base()
        {
        }

        public IEnumerable<Practice> GetAll()
        {
            return Context.Practice.AsEnumerable();
        }

        public IEnumerable<Practice> GetAllWithAliases()
        {
            return Context.Practice.Include(p => p.PracticeAlias).AsEnumerable();
        }

        public Practice GetBy(int id)
        {
            return Context.Practice.FirstOrDefault(p => p.PraId == id);
        }

        public void Put(Practice practice)
        {
            Context.Practice.Add(practice);

            Context.SaveChanges();
        }

        public void Update(Practice practice)
        {
            Context.Practice.Update(practice);

            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var practice = new Practice() { PraId = id };
            
            Context.Practice.Attach(practice);

            Context.Practice.Remove(practice);
                
            Context.SaveChanges();
        }
    }
}
